package test.Notifikasi;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Notifikasi_Page;
import utilities.BaseTest;

public class Test_Notifikasi_Cari_Positive extends BaseTest {
    Notifikasi_Page Notif;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_Notif() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Notif = new Notifikasi_Page(driver);
        click(Notif.navnotif());
        click(Notif.navnotif2());
        type(Notif.isijudulnotif(), "Penilaian apli");
        click(Notif.tombolcari());
        Assert.assertEquals(getText(Notif.textditemukan()), "Survey Baru: Penilaian aplikasi Sapawarga");
    }

    @Test
    public void Cari_NotifStatus() throws Exception {
        click(Notif.tombolreset());
        type(Notif.isijudulnotif(), "Penilaian apli");
        click(Notif.tombolcari());
        click(Notif.pilihstatus());
        click(Notif.pilihstatus2());
        Assert.assertEquals(getText(Notif.textditemukan()), "Survey Baru: Penilaian aplikasi Sapawarga");
        Assert.assertEquals(getText(Notif.textstatusditemukan()), "Dipublikasikan");
    }

    @Test
    public void Cari_NotifStatusKabKota() throws Exception {
        click(Notif.tombolreset());
        type(Notif.isijudulnotif(), "Penilaian apli");
        click(Notif.tombolcari());
        click(Notif.pilihstatus());
        click(Notif.pilihstatus2());
        click(Notif.pilihkotakab());
        verify(Notif.pilihkotakab());
        click(Notif.pilihkotakab2());
        Assert.assertEquals(getText(Notif.textditemukan()), "Survey Baru: Penilaian aplikasi Sapawarga");
        Assert.assertEquals(getText(Notif.textstatusditemukan()), "Dipublikasikan");
    }

    @Test
    public void Cari_NotifStatusKabKotaKec() throws Exception {
        click(Notif.tombolreset());
        type(Notif.isijudulnotif(), "Penilaian apli");
        click(Notif.tombolcari());
        click(Notif.pilihstatus());
        click(Notif.pilihstatus2());
        click(Notif.pilihkotakab());
        verify(Notif.pilihkotakab());
        click(Notif.pilihkotakab2());
        click(Notif.pilihkec());
        verify(Notif.pilihkec());
        click(Notif.pilihkec2());
        Assert.assertEquals(getText(Notif.textditemukan()), "Survey Baru: Penilaian aplikasi Sapawarga");
        Assert.assertEquals(getText(Notif.textstatusditemukan()), "Dipublikasikan");
    }

    @Test
    public void Cari_NotifStatusKabKotaKecKelDesa() throws Exception {
        click(Notif.tombolreset());
        type(Notif.isijudulnotif(), "Penilaian apli");
        click(Notif.tombolcari());
        click(Notif.pilihstatus());
        click(Notif.pilihstatus2());
        click(Notif.pilihkotakab());
        verify(Notif.pilihkotakab());
        click(Notif.pilihkotakab2());
        click(Notif.pilihkec());
        verify(Notif.pilihkec());
        click(Notif.pilihkec2());
        click(Notif.pilihkeldesa());
        verify(Notif.pilihkec2());
        click(Notif.pilihkeldesa2());
        Assert.assertEquals(getText(Notif.textditemukan()), "Survey Baru: Penilaian aplikasi Sapawarga");
        Assert.assertEquals(getText(Notif.textstatusditemukan()), "Dipublikasikan");
    }

    @Test
    public void Cari_NotifKabKota() throws Exception {
        click(Notif.tombolreset());
        type(Notif.isijudulnotif(), "Penilaian apli");
        click(Notif.tombolcari());
        click(Notif.pilihkotakab());
        verify(Notif.pilihkotakab());
        click(Notif.pilihkotakab2());
        Assert.assertEquals(getText(Notif.textditemukan()), "Survey Baru: Penilaian aplikasi Sapawarga");
        Assert.assertEquals(getText(Notif.textstatusditemukan()), "Dipublikasikan");
    }

    @Test
    public void Cari_NotifKabKotaKec() throws Exception {
        click(Notif.tombolreset());
        type(Notif.isijudulnotif(), "Penilaian apli");
        click(Notif.tombolcari());
        click(Notif.pilihkotakab());
        verify(Notif.pilihkotakab());
        click(Notif.pilihkotakab2());
        click(Notif.pilihkec());
        verify(Notif.pilihkec());
        click(Notif.pilihkec2());
        Assert.assertEquals(getText(Notif.textditemukan()), "Survey Baru: Penilaian aplikasi Sapawarga");
        Assert.assertEquals(getText(Notif.textstatusditemukan()), "Dipublikasikan");
    }

    @Test
    public void Cari_NotifKabKotaKecKelDesa() throws Exception {
        click(Notif.tombolreset());
        type(Notif.isijudulnotif(), "Penilaian apli");
        click(Notif.tombolcari());
        click(Notif.pilihkotakab());
        verify(Notif.pilihkotakab());
        click(Notif.pilihkotakab2());
        click(Notif.pilihkec());
        verify(Notif.pilihkec());
        click(Notif.pilihkec2());
        click(Notif.pilihkeldesa());
        verify(Notif.pilihkec2());
        click(Notif.pilihkeldesa2());
        Assert.assertEquals(getText(Notif.textditemukan()), "Survey Baru: Penilaian aplikasi Sapawarga");
        Assert.assertEquals(getText(Notif.textstatusditemukan()), "Dipublikasikan");
    }
}
