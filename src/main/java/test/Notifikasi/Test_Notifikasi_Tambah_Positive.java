package test.Notifikasi;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Notifikasi_Page;
import utilities.BaseTest;

public class Test_Notifikasi_Tambah_Positive extends BaseTest {
    Notifikasi_Page Notif;
    Dashboard_Page dashboardSW;

    @Test
    public void Tambah_Notif() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Notif = new Notifikasi_Page(driver);
        click(Notif.navnotif());
        click(Notif.navnotif2());
        click(Notif.tomboltambahnotif());
        typeRandom(Notif.isitambahjudul(),"Berita Pilihan Hari ini");
        click(Notif.pilihtambahkategori());
        click(Notif.pilihtambahkategori2());
        type(Notif.isipesan(),"Himbawan kepada masyarakat jawabarat peduli akan wabah corona");
        click(Notif.kirimpesan());
        click(Notif.konfya());
        verify(Notif.halamanutamaditemukan());
    }
}
