package test.ReleaseManagement;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.ReleaseManagement_Page;
import utilities.BaseTest;

public class Test_Release_Hapus_Positive extends BaseTest {
    ReleaseManagement_Page Release;
    Dashboard_Page dashboardSW;

    @Test
    public void Hapus_Release() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Release = new ReleaseManagement_Page(driver);
        click(Release.nav());
        click(Release.tombolhapus());
        click(Release.pilihya());
        verify(Release.tomboltambah());

    }
}