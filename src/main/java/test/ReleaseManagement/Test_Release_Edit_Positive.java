package test.ReleaseManagement;

import org.openqa.selenium.Keys;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.ReleaseManagement_Page;
import utilities.BaseTest;

public class Test_Release_Edit_Positive extends BaseTest {
    ReleaseManagement_Page Release;
    Dashboard_Page dashboardSW;

    @Test
    public void Edit_Release() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Release = new ReleaseManagement_Page(driver);
        click(Release.nav());
        click(Release.tomboledit());
        type(Release.isiversi(),"3");
        type(Release.isiversi(),"3333");
        click(Release.pilihya());
        click(Release.tombolsimpan());
        verify(Release.tomboledit());
    }
}