package test.Kategori;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Kategori_Page;
import utilities.BaseTest;

public class Test_Kategori_Tambah_Positive extends BaseTest {
    Kategori_Page Kategori;
    Dashboard_Page dashboardSW;

    @Test
    public void Tambah_Kategori() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Kategori = new Kategori_Page(driver);
        click(Kategori.navkategori());
        click(Kategori.navkategori2());
        click(Kategori.tomboltambahkategori());
        typeRandom(Kategori.isikategori(),"Perdagangan");
        click(Kategori.pilihfitur());
        click(Kategori.pilihfitur2());
        click(Kategori.tombolsimpan());
        verify(Kategori.tombolcari());
    }
}