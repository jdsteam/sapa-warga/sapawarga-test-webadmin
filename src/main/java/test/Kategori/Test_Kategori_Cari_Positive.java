package test.Kategori;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Kategori_Page;
import utilities.BaseTest;

public class Test_Kategori_Cari_Positive extends BaseTest {
    Kategori_Page Kategori;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_Kategori() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Kategori = new Kategori_Page(driver);
        click(Kategori.navkategori());
        click(Kategori.navkategori2());
        type(Kategori.isikategoricari(), "Sosial");
        click(Kategori.tombolcari());
        Assert.assertEquals(getText(Kategori.texttemukan()), "Sosialisasi");
    }
}
