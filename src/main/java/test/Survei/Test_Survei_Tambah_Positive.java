package test.Survei;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Survei_Page;
import utilities.BaseTest;
import org.testng.Assert;


public class Test_Survei_Tambah_Positive extends BaseTest {
    Survei_Page Survei;
    Dashboard_Page dashboardSW;

    @Test
    public void Kirim_Survei() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Survei = new Survei_Page(driver);
        click(Survei.kliknavigasi());
        click(Survei.kliknavigasi2());
        click(Survei.kliktambahsurvei());
        typeRandom(Survei.isitambahjudul(),"Survei Pendataan Daerah Terpapar Corona");
        click(Survei.pilihtambahkategori());
        click(Survei.pilihtambahkategori2());
        click(Survei.pilihtidak());
        type(Survei.isiurl(),"https://sapawarga-staging.jabarprov.go.id/#/survey/create");
        type(Survei.isiurl2(),"https://sapawarga-staging.jabarprov.go.id/#/survey/create");
        click(Survei.kirimpesan());
        //click(Survei.konfya());
        verify(Survei.halaman());
    }
}
