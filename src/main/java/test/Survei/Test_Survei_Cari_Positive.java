package test.Survei;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Survei_Page;
import utilities.BaseTest;
import org.testng.Assert;


public class Test_Survei_Cari_Positive extends BaseTest {
    Survei_Page Survei;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_Survei() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Survei = new Survei_Page(driver);
        click(Survei.kliknavigasi());
        click(Survei.kliknavigasi2());
        type(Survei.isicarisurvei(),"Saran dan harapan");
        click(Survei.tombolcarisurvei());
        Assert.assertEquals(getText(Survei.surveiditemukan()),"Saran dan harapan pengguna Sapawarga");
    }
    @Test
    public void Cari_SurveiStatus() throws Exception {
        click(Survei.tombolresetcari());
        type(Survei.isicarisurvei(),"Saran dan harapan");
        click(Survei.tombolcarisurvei());
        click(Survei.pilihstatus());
        click(Survei.pilihstatus2());
        click(Survei.tombolcarisurvei());
        Assert.assertEquals(getText(Survei.surveiditemukan()),"Saran dan harapan pengguna Sapawarga");
        Assert.assertEquals(getText(Survei.statusditemukan()),"Berakhir");
    }
}
