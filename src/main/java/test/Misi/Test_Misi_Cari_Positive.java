package test.Misi;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Misi_Page;
import utilities.BaseTest;

public class Test_Misi_Cari_Positive extends BaseTest {
    Misi_Page Misi;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_Misi() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Misi = new Misi_Page(driver);
        click(Misi.navmisi());
        type(Misi.isijudulmisi(), "Misi");
        click(Misi.tombolcari());
        Assert.assertEquals(getText(Misi.textditemukan()), "Misi info penting");
    }
}
