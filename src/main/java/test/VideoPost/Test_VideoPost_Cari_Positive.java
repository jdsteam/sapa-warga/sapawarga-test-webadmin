package test.VideoPost;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.VideoPost_Page;
import utilities.BaseTest;

public class Test_VideoPost_Cari_Positive extends BaseTest {
    VideoPost_Page Video;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_VideoPost() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Video = new VideoPost_Page(driver);
        click(Video.clickvideo());
        click(Video.clickvideo2());
        type(Video.isijudulvideo(),"Peresmian launching Chanel Pembayaran PBB Melalui Tokopedia dan Bukalapak");
        click(Video.clickcari());
        Assert.assertEquals(getText(Video.videoditemukan()),"Peresmian launching Chanel Pembayaran PBB Melalui Tokopedia dan Bukalapak");

    }
}
