package test.VideoPost;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.VideoPost_Page;
import utilities.BaseTest;

public class Test_VideoPost_Tambah_Positive extends BaseTest {
    VideoPost_Page Video;
    Dashboard_Page dashboardSW;

    @Test
    public void Tambah_VideoPost() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Video = new VideoPost_Page(driver);
        click(Video.clickvideo());
        click(Video.clickvideo2());
        click(Video.clicktambahvideopost());
        typeRandom(Video.isijudul(),"Jalan-jalan di Geopark Ciletuh");
        click(Video.pilihkategori());
        click(Video.pilihkategori2());
        click(Video.pilihtarget());
        click(Video.pilihtarget2());
        click(Video.pilihsetprioritas());
        click(Video.pilihsetprioritas2());
        type(Video.isiurl(),"https://www.youtube.com/watch?v=-sZKo_G0RYw");
        click(Video.tomboltambahvideo());
        verify(Video.clickcari());
    }
}
