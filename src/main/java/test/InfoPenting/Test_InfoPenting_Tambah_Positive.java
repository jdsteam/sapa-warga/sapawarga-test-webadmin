package test.InfoPenting;

import org.testng.Assert;
import page.Dashboard_Page;
import page.InfoPenting_Page;
import utilities.BaseTest;
import org.testng.annotations.Test;


public class Test_InfoPenting_Tambah_Positive extends BaseTest{
    InfoPenting_Page Info;
    Dashboard_Page dashboardSW;
    @Test
    public void Tambah_InfoPenting() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Info = new InfoPenting_Page(driver);
        click(Info.clickinfopenting());
        click(Info.clickinfopenting2());
        click(Info.clicktambahinfopenting());
        typeRandom((Info.isijudul()),"Tiket Kereta Api Masa Lebaran Bisa Dipesan Mulai 14 Februari 2020");
        click(Info.pilihKategori());
        click(Info.pilihKategori2());
        click(Info.pilihtarget());
        click(Info.pilihtarget2());
        typeMce("Test doang kali aja bisaaa");
        type((Info.isilink()),("https://kai.id/information/full_news/3199-tiket-kereta-api-masa-lebaran-bisa-dipesan-mulai-14-februari-2020"));
        click(Info.clicksubmitInfopenting());
//        wait_Low();
//        String isirandom = typeRandomSimpan(Info.isijudul(),"Tiket Kereta Api Masa", "simpan" );
//        click(Info.clikdetail());
//        Assert.assertEquals(getText(Info.cekjudul()),isirandom);
//        System.out.println(isirandom);
    }
}
