package test.KelolaPengguna;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.KelolaPengguna_Page;
import utilities.BaseTest;

public class Test_KelolaPengguna_Cari_Positive extends BaseTest {
    KelolaPengguna_Page Pengguna;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_User() throws Exception {

        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Pengguna = new KelolaPengguna_Page(driver);
        click(Pengguna.clickkelolapengguna());
        click(Pengguna.clickkelolapengguna2());
        type(Pengguna.isiusername(),"robotkab");
        click(Pengguna.clickcari());
        Assert.assertEquals(getText(Pengguna.userditemukan()),"robotkab.jds");
    }

}
