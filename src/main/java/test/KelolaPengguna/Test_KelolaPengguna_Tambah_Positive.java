package test.KelolaPengguna;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.KelolaPengguna_Page;
import utilities.BaseTest;
import com.github.javafaker.Faker;

import java.util.Locale;

public class Test_KelolaPengguna_Tambah_Positive extends BaseTest {
    KelolaPengguna_Page Pengguna;
    Dashboard_Page dashboardSW;

/*    @Test
    public void Tambah_User() throws Exception {
        Locale locale = new Locale("in-ID");
        Faker faker = new Faker(locale);
        String firstName = faker.name().firstName();

        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Pengguna = new KelolaPengguna_Page(driver);
        click(Pengguna.clickkelolapengguna());
        click(Pengguna.clickkelolapengguna2());
        click(Pengguna.clicktambahpengguna());
        System.out.print(firstName);
        type(Pengguna.isitambahusername(),(firstName));
        wait_High();
*/
    @Test
    public void Tambah_User() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Pengguna = new KelolaPengguna_Page(driver);
        click(Pengguna.clickkelolapengguna());
        click(Pengguna.clickkelolapengguna2());
        click(Pengguna.clicktambahpengguna());
        verify(Pengguna.halamanditemukan());
        typeRandom(Pengguna.isitambahusername(),"testuser");
        type(Pengguna.isitambahnamalengkap(),"Tester JDS");
        typeRandomEmail(Pengguna.isiemail(),"tester@gmail.com");
        type(Pengguna.isipassword(),"123456");
        type(Pengguna.isiconfmpass(),"123456");
        type(Pengguna.isitelfn(),"000123456");
        click(Pengguna.pilihrole());
        verify(Pengguna.pilihrole2());
        click(Pengguna.pilihrole2());
        click(Pengguna.pilihkabkota());
        verify(Pengguna.pilihkabkota2());
        click(Pengguna.pilihkabkota2());
        click(Pengguna.pilihkec());
        verify(Pengguna.pilihkec2());
        click(Pengguna.pilihkec2());
        click(Pengguna.pilihkeldesa());
        verify(Pengguna.pilihkeldesa2());
        click(Pengguna.pilihkeldesa2());
        typeRandom(Pengguna.isialamat(),"Kp Rambutan No ");
        click(Pengguna.pilihtomboltambah());
        verify(Pengguna.halamanditemukan());
    }

}
