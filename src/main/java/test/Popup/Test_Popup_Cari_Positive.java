package test.Popup;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Popup_Page;
import utilities.BaseTest;

public class Test_Popup_Cari_Positive extends BaseTest {
    Popup_Page Popup;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_Popup() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Popup = new Popup_Page(driver);
        click(Popup.nav());
        click(Popup.nav2());
        type(Popup.isijudulpopup(),"Program Unggulan");
        click(Popup.tombolcari());
        Assert.assertEquals(getText(Popup.popupditemukan()),"Program Unggulan Sapawarga");
    }
    @Test
    public void Cari_Popup_Kategori() throws Exception {
        click(Popup.tombolreset());
        type(Popup.isijudulpopup(),"Program Unggulan");
        click(Popup.pilihkategori());
        click(Popup.pilihkategori2());
        click(Popup.tombolcari());
        Assert.assertEquals(getText(Popup.popupditemukan()),"Program Unggulan Sapawarga");
    }
}