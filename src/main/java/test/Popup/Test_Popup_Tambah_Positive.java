package test.Popup;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Popup_Page;
import utilities.BaseTest;

public class Test_Popup_Tambah_Positive extends BaseTest {
    Popup_Page Popup;
    Dashboard_Page dashboardSW;

    @Test
    public void Tambah_Popup() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Popup = new Popup_Page(driver);
        click(Popup.nav());
        click(Popup.nav2());
        click(Popup.tomboltambah());
        typeRandom(Popup.isijudul(),"Situs Google Terkini");
        type(Popup.isiurl(),"https://www.google.com/");
        click(Popup.tomboltambahpopup());
        verify(Popup.tombolcari());
    }
}
