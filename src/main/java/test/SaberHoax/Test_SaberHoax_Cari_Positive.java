package test.SaberHoax;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.SaberHoax_Page;
import utilities.BaseTest;

public class Test_SaberHoax_Cari_Positive extends BaseTest {
    SaberHoax_Page SaberHoax;
    Dashboard_Page dashboardSW;
    @Test
    public void Cari_Saber() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        SaberHoax = new SaberHoax_Page(driver);
        click(SaberHoax.clickberita());
        click(SaberHoax.clickberita2());
        type(SaberHoax.isijudulberita(),"KEMENTERIAN LHK BAGIKAN");
        click(SaberHoax.clickcari());
        Assert.assertEquals(getText(SaberHoax.beritaditemukan()),"KEMENTERIAN LHK BAGIKAN BIBIT TANAMAN SECARA CUMA-CUMA, DENGAN SYARAT TIDAK DIPERJUAL BELIKAN");
    }
    @Test
    public void Cari_SaberStatus() throws Exception {
        click(SaberHoax.clickreset());
        type(SaberHoax.isijudulberita(),"KEMENTERIAN LHK BAGIKAN");
        click(SaberHoax.clickstatus());
        verify(SaberHoax.clickstatus());
        click(SaberHoax.clickstatus2());
        click(SaberHoax.clickcari());
        Assert.assertEquals(getText(SaberHoax.beritaditemukan()),"KEMENTERIAN LHK BAGIKAN BIBIT TANAMAN SECARA CUMA-CUMA, DENGAN SYARAT TIDAK DIPERJUAL BELIKAN");
        Assert.assertEquals(getText(SaberHoax.statusditemukan()),"Fakta");
    }
}

