package test.KegiatanRW;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.KegiatanRW_Page;
import utilities.BaseTest;

public class Test_KegiatanRW_Cari_Positive extends BaseTest {
    KegiatanRW_Page RW;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_KegiatanRW() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        RW = new KegiatanRW_Page(driver);
        click(RW.navRW());
        click(RW.navRW2());
        type(RW.isinamakegiatan(), "Demi keamanan ");
        click(RW.tombolcari());
        Assert.assertEquals(getText(RW.textditemukan()), "Demi keamanan kita bersama");
    }

    @Test
    public void Cari_KegiatanRWPengguna() throws Exception {
        click(RW.tombolreset());
        type(RW.isinamakegiatan(), "Demi keamanan ");
        type(RW.isinamapengguna(), "Staff");
        click(RW.tombolcari());
        Assert.assertEquals(getText(RW.textditemukan()), "Demi keamanan kita bersama");
        Assert.assertEquals(getText(RW.textpenggunatemukan()), "Staff RW");
    }

    @Test
    public void Cari_KegiatanRWPenggunaStatus() throws Exception {
        click(RW.tombolreset());
        click(RW.tombolreset());
        type(RW.isinamakegiatan(), "Demi keamanan ");
        type(RW.isinamapengguna(), "Staff");
        click(RW.pilihstatus());
        click(RW.pilihstatus2());
        click(RW.tombolcari());
        Assert.assertEquals(getText(RW.textditemukan()), "Demi keamanan kita bersama");
        Assert.assertEquals(getText(RW.textpenggunatemukan()), "Staff RW");
        Assert.assertEquals(getText(RW.textstatusditemukan()), "Aktif");
    }

    @Test
    public void Cari_KegiatanRWStatus() throws Exception {
        click(RW.tombolreset());
        click(RW.tombolreset());
        type(RW.isinamakegiatan(), "Demi keamanan ");
        click(RW.pilihstatus());
        click(RW.pilihstatus2());
        click(RW.tombolcari());
        Assert.assertEquals(getText(RW.textditemukan()), "Demi keamanan kita bersama");
        Assert.assertEquals(getText(RW.textpenggunatemukan()), "Staff RW");
        Assert.assertEquals(getText(RW.textstatusditemukan()), "Aktif");
    }

}