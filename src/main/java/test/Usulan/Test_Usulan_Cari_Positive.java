package test.Usulan;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Usulan_Page;
import utilities.BaseTest;

public class Test_Usulan_Cari_Positive extends BaseTest {
    Usulan_Page Usulan;
    Dashboard_Page dashboardSW;
    @Test
    public void Cari_Usulan() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Usulan = new Usulan_Page(driver);
        click(Usulan.clickusulan());
        click(Usulan.clickusulan2());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
    }

    @Test
    public void Cari_UsulanKategori() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkategori());
        click(Usulan.pilihkategori2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
    }
    @Test
    public void Cari_UsulanKategoriStatus() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkategori());
        click(Usulan.pilihkategori2());
        click(Usulan.pilihstatus());
        click(Usulan.pilihstatus2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKategoriStatusKabKota() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkategori());
        click(Usulan.pilihkategori2());
        click(Usulan.pilihstatus());
        click(Usulan.pilihstatus2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKategoriStatusKabKotaKec() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkategori());
        click(Usulan.pilihkategori2());
        click(Usulan.pilihstatus());
        click(Usulan.pilihstatus2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        click(Usulan.pilihkec());
        verify(Usulan.pilihkec());
        click(Usulan.pilihkec2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKategoriStatusKabKotaKecKelDesa() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkategori());
        click(Usulan.pilihkategori2());
        click(Usulan.pilihstatus());
        click(Usulan.pilihstatus2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        click(Usulan.pilihkec());
        verify(Usulan.pilihkec());
        click(Usulan.pilihkec2());
        click(Usulan.pilihkeldesa());
        verify(Usulan.pilihkeldesa());
        click(Usulan.pilihkeldesa2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKabKota() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKabKotaKec() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        click(Usulan.pilihkec());
        verify(Usulan.pilihkec());
        click(Usulan.pilihkec2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKabKotaKecKelDesa() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        click(Usulan.pilihkec());
        verify(Usulan.pilihkec());
        click(Usulan.pilihkec2());
        click(Usulan.pilihkeldesa());
        verify(Usulan.pilihkeldesa());
        click(Usulan.pilihkeldesa2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKategoriKabKota() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkategori());
        click(Usulan.pilihkategori2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKategoriKabKotaKec() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkategori());
        click(Usulan.pilihkategori2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        click(Usulan.pilihkec());
        verify(Usulan.pilihkec());
        click(Usulan.pilihkec2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanKategoriKabKotaKecKelDesa() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihkategori());
        click(Usulan.pilihkategori2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        click(Usulan.pilihkec());
        verify(Usulan.pilihkec());
        click(Usulan.pilihkec2());
        click(Usulan.pilihkeldesa());
        click(Usulan.pilihkeldesa2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanStatusKabKota() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihstatus());
        click(Usulan.pilihstatus2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanStatusKabKotaKec() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihstatus());
        click(Usulan.pilihstatus2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        click(Usulan.pilihkec());
        verify(Usulan.pilihkec());
        click(Usulan.pilihkec2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
    @Test
    public void Cari_UsulanStatusKabKotaKecKelDesa() throws Exception {
        click(Usulan.clickreset());
        type(Usulan.isijudulusulan(), "coba lapor 2");
        click(Usulan.clickcari());
        click(Usulan.pilihstatus());
        click(Usulan.pilihstatus2());
        click(Usulan.pilihstatus());
        click(Usulan.pilihstatus2());
        click(Usulan.pilihkabkota());
        verify(Usulan.pilihkabkota());
        click(Usulan.pilihkabkota2());
        click(Usulan.pilihkec());
        verify(Usulan.pilihkec());
        click(Usulan.pilihkec2());
        click(Usulan.pilihkeldesa());
        verify(Usulan.pilihkeldesa());
        click(Usulan.pilihkeldesa2());
        Assert.assertEquals(getText(Usulan.usulanditemukan()),"coba lapor 2");
        Assert.assertEquals(getText(Usulan.kategoriditemukan()),"Infrastruktur");
        Assert.assertEquals(getText(Usulan.statusditemukan()),"Tidak Dipublikasikan");
    }
}
