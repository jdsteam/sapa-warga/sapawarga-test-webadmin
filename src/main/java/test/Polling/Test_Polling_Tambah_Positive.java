package test.Polling;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Polling_Page;
import utilities.BaseTest;


public class Test_Polling_Tambah_Positive extends BaseTest {
    Polling_Page Polling;
    Dashboard_Page dashboardSW;

    @Test
    public void Kirim_Polling() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Polling = new Polling_Page(driver);
        click(Polling.kliknavigasi());
        click(Polling.kliknavigasi2());
        click(Polling.kliktambahpolling());
        typeRandom(Polling.isitambahjudul(),"Polling Informasi Daerah Terpapar Corona");
        click(Polling.pilihtambahkategori());
        click(Polling.pilihtambahkategori2());
        type(Polling.isitambahdeskripsi(),"Untuk mengetahui daerah yang terpapar virus corona");
        type(Polling.isitambahpengantar(),"Untuk mengetahui daerah yang terpapar virus corona");
        type(Polling.isipertanyaan(),"Apakah Di Wilayah Anda Terdapat Kasus Corona?");
        click(Polling.pilihtidak());
        click(Polling.pilihcustom());
        click(Polling.pilihjawabanlain());
        click(Polling.pilihjawabanlain());
        type(Polling.isijawabancustom(),"Ya");
        type(Polling.isijawabancustom2(),"Tidak Tahu");
        type(Polling.isijawabancustom3(),"Belum ada");
        click(Polling.publikasikanpolling());
        click(Polling.konfya());
        verify(Polling.halamanutamaditemukan());
    }
}
