package test.Polling;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Polling_Page;
import utilities.BaseTest;
import org.testng.Assert;


public class Test_Polling_Cari_Positive extends BaseTest {
    Polling_Page Polling;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_Polling() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Polling = new Polling_Page(driver);
        click(Polling.kliknavigasi());
        click(Polling.kliknavigasi2());
        type(Polling.isicaripolling(),"naon");
        click(Polling.tombolcaripolling());
        Assert.assertEquals(getText(Polling.pollingditemukan()),"Poling naon atuh");
    }
    @Test
    public void Cari_PollingStatus() throws Exception {
        click(Polling.tombolresetcari());
        type(Polling.isicaripolling(),"naon");
        click(Polling.tombolcaripolling());
        click(Polling.pilihstatus());
        click(Polling.pilihstatus2());
        click(Polling.tombolcaripolling());
        Assert.assertEquals(getText(Polling.pollingditemukan()),"Poling naon atuh");
        Assert.assertEquals(getText(Polling.statusditemukan()),"Berakhir");
    }
    @Test
    public void Cari_PollingStatusKabKota() throws Exception {
        click(Polling.tombolresetcari());
        type(Polling.isicaripolling(),"naon");
        click(Polling.tombolcaripolling());
        click(Polling.pilihstatus());
        click(Polling.pilihstatus2());
        click(Polling.pilihkabkota());
        verify(Polling.pilihkabkota());
        click(Polling.pilihkabkota2());
        click(Polling.tombolcaripolling());
        Assert.assertEquals(getText(Polling.pollingditemukan()),"Poling naon atuh");
        Assert.assertEquals(getText(Polling.statusditemukan()),"Berakhir");
    }
    @Test
    public void Cari_PollingStatusKabKotaKec() throws Exception {
        click(Polling.tombolresetcari());
        type(Polling.isicaripolling(),"naon");
        click(Polling.tombolcaripolling());
        click(Polling.pilihstatus());
        click(Polling.pilihstatus2());
        click(Polling.pilihkabkota());
        verify(Polling.pilihkabkota());
        click(Polling.pilihkabkota2());
        click(Polling.pilihkec());
        verify(Polling.pilihkec());
        click(Polling.pilihkec2());
        click(Polling.tombolcaripolling());
        Assert.assertEquals(getText(Polling.pollingditemukan()),"Poling naon atuh");
        Assert.assertEquals(getText(Polling.statusditemukan()),"Berakhir");
    }

    @Test
    public void Cari_PollingStatusKabKotaKecKel() throws Exception {
        click(Polling.tombolresetcari());
        type(Polling.isicaripolling(),"naon");
        click(Polling.tombolcaripolling());
        click(Polling.pilihstatus());
        verify(Polling.pilihstatus());
        click(Polling.pilihstatus2());
        click(Polling.pilihkabkota());
        verify(Polling.pilihkabkota());
        click(Polling.pilihkabkota2());
        click(Polling.pilihkec());
        verify(Polling.pilihkec());
        click(Polling.pilihkec2());
        click(Polling.pilihkeldesa());
        verify(Polling.pilihkeldesa());
        click(Polling.pilihkeldesa2());
        click(Polling.tombolcaripolling());
        Assert.assertEquals(getText(Polling.pollingditemukan()),"Poling naon atuh");
        Assert.assertEquals(getText(Polling.statusditemukan()),"Berakhir");
    }
}
