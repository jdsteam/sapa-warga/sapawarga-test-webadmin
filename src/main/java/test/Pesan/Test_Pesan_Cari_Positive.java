package test.Pesan;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Pesan_Page;
import utilities.BaseTest;

public class Test_Pesan_Cari_Positive extends BaseTest {
    Pesan_Page pesan;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_Pesan() throws Exception {

        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        pesan = new Pesan_Page(driver);
        click(pesan.kliknavigasi());
        type(pesan.isicaripesan(), "Sosialisasi program");
        click(pesan.tombolcaripesan());
        Assert.assertEquals(getText(pesan.pesanditemukan()),"Sosialisasi program kerja Gubernur Jawa Barat");
        Assert.assertEquals(getText(pesan.statusditemukan()),"Dipublikasikan");
    }
    @Test
    public void Cari_PesanStatus() throws Exception {
        click(pesan.tombolresetcari());
        type(pesan.isicaripesan(), "Sosialisasi program");
        click(pesan.pilihstatus());
        click(pesan.pilihstatus2());
        click(pesan.tombolcaripesan());
        Assert.assertEquals(getText(pesan.pesanditemukan()),"Sosialisasi program kerja Gubernur Jawa Barat");
        Assert.assertEquals(getText(pesan.statusditemukan()),"Dipublikasikan");
    }
    @Test
    public void Cari_PesanStatusKabKota() throws Exception {
        click(pesan.tombolresetcari());
        type(pesan.isicaripesan(), "Sosialisasi program");
        click(pesan.pilihstatus());
        click(pesan.pilihstatus2());
        click(pesan.pilihkabkota());
        click(pesan.pilihkabkota2());
        click(pesan.tombolcaripesan());
        Assert.assertEquals(getText(pesan.pesanditemukan()),"Sosialisasi program kerja Gubernur Jawa Barat");
        Assert.assertEquals(getText(pesan.statusditemukan()),"Dipublikasikan");
    }
    @Test
    public void Cari_PesanStatusKabKotaKec() throws Exception {
        click(pesan.tombolresetcari());
        type(pesan.isicaripesan(), "Sosialisasi program");
        click(pesan.pilihstatus());
        click(pesan.pilihstatus2());
        click(pesan.pilihkabkota());
        click(pesan.pilihkabkota2());
        click(pesan.pilihkec());
        click(pesan.pilihkec2());
        click(pesan.tombolcaripesan());
        Assert.assertEquals(getText(pesan.pesanditemukan()),"Sosialisasi program kerja Gubernur Jawa Barat");
        Assert.assertEquals(getText(pesan.statusditemukan()),"Dipublikasikan");
    }
    @Test
    public void Cari_PesanStatusKabKotaKecKelDesa() throws Exception {
        click(pesan.tombolresetcari());
        type(pesan.isicaripesan(), "Sosialisasi program");
        click(pesan.pilihstatus());
        click(pesan.pilihstatus2());
        click(pesan.pilihkabkota());
        click(pesan.pilihkabkota2());
        click(pesan.pilihkec());
        click(pesan.pilihkec2());
        click(pesan.pilihkeldesa());
        click(pesan.pilihkeldesa2());
        click(pesan.tombolcaripesan());
        Assert.assertEquals(getText(pesan.pesanditemukan()),"Sosialisasi program kerja Gubernur Jawa Barat");
        Assert.assertEquals(getText(pesan.statusditemukan()),"Dipublikasikan");
    }
    @Test
    public void Cari_PesanKabKota() throws Exception {
        click(pesan.tombolresetcari());
        type(pesan.isicaripesan(), "Sosialisasi program");
        click(pesan.pilihkabkota());
        click(pesan.pilihkabkota2());
        click(pesan.tombolcaripesan());
        Assert.assertEquals(getText(pesan.pesanditemukan()),"Sosialisasi program kerja Gubernur Jawa Barat");
        Assert.assertEquals(getText(pesan.statusditemukan()),"Dipublikasikan");
    }
    @Test
    public void Cari_PesanKabKotaKec() throws Exception {
        click(pesan.tombolresetcari());
        type(pesan.isicaripesan(), "Sosialisasi program");
        click(pesan.pilihkabkota());
        click(pesan.pilihkabkota2());
        click(pesan.pilihkec());
        click(pesan.pilihkec2());
        click(pesan.tombolcaripesan());
        Assert.assertEquals(getText(pesan.pesanditemukan()),"Sosialisasi program kerja Gubernur Jawa Barat");
        Assert.assertEquals(getText(pesan.statusditemukan()),"Dipublikasikan");
    }
    @Test
    public void Cari_PesanKabKotaKecKelDesa() throws Exception {
        click(pesan.tombolresetcari());
        type(pesan.isicaripesan(), "Sosialisasi program");
        click(pesan.pilihkabkota());
        click(pesan.pilihkabkota2());
        click(pesan.pilihkec());
        click(pesan.pilihkec2());
        click(pesan.pilihkeldesa());
        click(pesan.pilihkeldesa2());
        click(pesan.tombolcaripesan());
        Assert.assertEquals(getText(pesan.pesanditemukan()),"Sosialisasi program kerja Gubernur Jawa Barat");
        Assert.assertEquals(getText(pesan.statusditemukan()),"Dipublikasikan");
    }
}
