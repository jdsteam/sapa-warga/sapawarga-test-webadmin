package test.Pesan;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.Pesan_Page;
import utilities.BaseTest;

public class Test_Pesan_Tambah_Positive extends BaseTest {
    Pesan_Page pesan;
    Dashboard_Page dashboardSW;

    @Test
    public void Kirim_Pesan() throws Exception {

        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        pesan = new Pesan_Page(driver);
        click(pesan.kliknavigasi());
        click(pesan.tomboltambahpesan());
        typeRandom(pesan.isitambahjudul(),"Himbauan masyarakat jawabarat");
        click(pesan.pilihtambahkategori());
        click(pesan.pilihtambahkategori2());
        typeMce("Himbawan kepada masyarakat jawabarat peduli akan wabah corona");
        click(pesan.kirimpesan());
        click(pesan.konfya());
        verify(pesan.halamanutamaditemukan());
    }
}
