package test.SumberBerita;

import org.testng.annotations.Test;
import page.Dashboard_Page;
import page.SumberBerita_Page;
import utilities.BaseTest;

public class Test_SumberBerita_Tambah_Positive extends BaseTest {
    SumberBerita_Page SumberBerita;
    Dashboard_Page dashboardSW;

    @Test
    public void Tambah_Sumber() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        SumberBerita = new SumberBerita_Page(driver);
        click(SumberBerita.navkategori());
        click(SumberBerita.navkategori2());
        click(SumberBerita.tomboltambahsumber());
        typeRandom(SumberBerita.isijudulsumber(),"Perdagangan");
        typeRandom(SumberBerita.isitautanlogo(),"https://www.google.com/");
        typeRandom(SumberBerita.isitautan(),"https://www.google.com/");
        click(SumberBerita.tombolsimpan());
        verify(SumberBerita.tomboltambahsumber());
    }
}