package test.NomorPenting;

import org.testng.Assert;
import page.Dashboard_Page;
import page.NomorPenting_Page;
import utilities.BaseTest;
import org.testng.annotations.Test;

public class Test_NomorPenting_Cari_Positive extends BaseTest {
        NomorPenting_Page Nomor;
        Dashboard_Page dashboardSW;

    @Test
    public void Cari_NomorPenting() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Nomor = new NomorPenting_Page(driver);
        click(Nomor.clicknomorpenting());
        click(Nomor.clicknomorpenting2());
        type(Nomor.isiinstansi(),"AREN JAYA");
        click(Nomor.clickcari());
        Assert.assertEquals(getText(Nomor.nomorditemukan()),"AREN JAYA");
    }
    @Test
    public void Cari_NomorPentingInstansiNomor() throws Exception {
        click(Nomor.clickreset());
        type(Nomor.isiinstansi(), "AREN JAYA");
        type(Nomor.isitelfn(), "02188347036");
        click(Nomor.clickcari());
        Assert.assertEquals(getText(Nomor.nomorditemukan()), "AREN JAYA");
        Assert.assertEquals(getText(Nomor.nomorditemukan2()), "02188347036");
    }
}
