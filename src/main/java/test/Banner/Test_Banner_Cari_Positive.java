package test.Banner;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Banner_Page;
import page.Dashboard_Page;
import utilities.BaseTest;

public class Test_Banner_Cari_Positive extends BaseTest {
    Banner_Page Banner;
    Dashboard_Page dashboardSW;

    @Test
    public void Cari_Banner() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Banner = new Banner_Page(driver);
        click(Banner.navbanner());
        click(Banner.navbanner2());
        type(Banner.isijudulbanner(), "Banner ulang tahun");
        click(Banner.tombolcari());
        Assert.assertEquals(getText(Banner.textditemukan()), "Banner ulang tahun Jawa Barat");
    }

    @Test
    public void Cari_BannerKategori() throws Exception {
        click(Banner.tombolreset());
        type(Banner.isijudulbanner(), "Banner ulang tahun");
        click(Banner.pilihkategori());
        click(Banner.pilihkategori2());
        click(Banner.tombolcari());
        Assert.assertEquals(getText(Banner.textditemukan()), "Banner ulang tahun Jawa Barat");
        Assert.assertEquals(getText(Banner.textkategoriditemukan()), "internal");
    }
 /*   @Test
    public void Cari_BannerKategoriStatus() throws Exception {
        click(Banner.tombolreset());
        type(Banner.isijudulbanner(),"Banner ulang tahun");
        click(Banner.pilihkategori());
        click(Banner.pilihkategori2());
        click(Banner.pilihstatus());
        verify(Banner.pilihstatus());
        click(Banner.pilihstatus2());
        Assert.assertEquals(getText(Banner.textditemukan()),"Banner ulang tahun Jawa Barat");
        Assert.assertEquals(getText(Banner.textkategoriditemukan()),"internal");
        Assert.assertEquals(getText(Banner.textstatusditemukan()),"Aktif");
    }
    @Test
    public void Cari_BannerStatus() throws Exception {
        click(Banner.tombolreset());
        type(Banner.isijudulbanner(),"Banner ulang tahun");
        click(Banner.pilihstatus());
        verify(Banner.pilihstatus());
        click(Banner.pilihstatus2());
        Assert.assertEquals(getText(Banner.textditemukan()),"Banner ulang tahun Jawa Barat");
        Assert.assertEquals(getText(Banner.textkategoriditemukan()),"internal");
        Assert.assertEquals(getText(Banner.textstatusditemukan()),"Aktif");
    }*/
}

