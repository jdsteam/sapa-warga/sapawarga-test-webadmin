package test.Berita;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Berita_Page;
import page.Dashboard_Page;
import utilities.BaseTest;

public class Test_Berita_Cari_Positive extends BaseTest {
    Berita_Page Berita;
    Dashboard_Page dashboardSW;
    @Test
    public void Cari_Berita() throws Exception {
        LoginSW("Admin");
        dashboardSW = new Dashboard_Page(driver);
        verify(dashboardSW.getlogo());
        Berita = new Berita_Page(driver);
        click(Berita.navberita());
        click(Berita.navberita2());
        type(Berita.isijudulberita(),"Pembebasan Lahan Tol");
        click(Berita.tombolcari());
        Assert.assertEquals(getText(Berita.textditemukan()),"Pembebasan Lahan Tol Cisumdawu Terhambat, Ini Alasan Wagub Jabar");
    }
}
