package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import io.qameta.allure.Step;
import utilities.BaseTest;

public class Login_Page extends BaseTest {
	
	public Login_Page(WebDriver driver) {
		this.driver = driver;
	}
	
	By username = By.name("username");
	By password = By.name("password");
	By tlogin = By.xpath("//*[@id=\"app\"]/div/form/button");

	public By isiUsername() { return username; }
	public By isiPassword() { return password; }
	public By clicklogin() { return tlogin; }

	
}
