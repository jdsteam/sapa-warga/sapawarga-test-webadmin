package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class KegiatanRW_Page extends BaseTest {

    public KegiatanRW_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasi = By.xpath("//span[contains(.,\'Aspirasi\')]");
    By navigasi2 = By.xpath("//span[contains(.,\'Kegiatan RW\')]");
    By namakegiatan = By.xpath("//input[@type=\'text\']");
    By cari = By.xpath("//button[contains(.,\'Cari\')]");
    By reset = By.xpath("//button[contains(.,\'Reset\')]");
    By username = By.xpath("(//input[@type=\'text\'])[2]");
    By status = By.xpath("(//input[@type=\'text\'])[3]");
    By status2 = By.xpath("//li[contains(.,\'Aktif\')]");
    public By navRW() { return navigasi; }
    public By navRW2() { return navigasi2; }
    public By tombolcari() { return cari; }
    public By tombolreset() { return reset; }
    public By isinamakegiatan() { return namakegiatan; }
    public By isinamapengguna() { return username; }
    public By pilihstatus() { return status; }
    public By pilihstatus2() { return status2; }

    //Hasil Pencarian
    By tidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By ditemukan = By.xpath("//td[contains(.,\'Demi keamanan kita bersama\')]");
    By statusditemukan = By.xpath("//td[contains(.,\'Aktif\')]");
    By usernameditemukan = By.xpath("//td[contains(.,\'Staff RW\')]");
    public By texttidakditemukan() { return tidakditemukan; }
    public By textditemukan() { return ditemukan; }
    public By textstatusditemukan() { return statusditemukan; }
    public By textpenggunatemukan() { return usernameditemukan; }
}
