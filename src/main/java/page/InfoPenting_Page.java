package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class InfoPenting_Page extends BaseTest {

    public InfoPenting_Page(WebDriver driver) {
        this.driver = driver;
    }

    //Halaman List Informasi Penting
    By navigasiinfopenting = By.xpath("//span[contains(.,\'Informasi\')]");
    By navigasiinfopenting2 = By.xpath("//span[contains(.,\'Info Penting\')]");
    By judulcari = By.xpath("//input[@type=\'text\']");
    By tombolcariinfopenting = By.xpath("//button[contains(.,\'Cari\')]");
    By tombolresetinfopenting = By.xpath("//button[contains(.,\'Reset\')]");
    By tombolTambahInfopenting = By.xpath("//span[contains(.,\' Tambah Info Penting\')]");
    By tombolDetailInformasi = By.xpath("(//button[@type=\'button\'])[4]");

    public By clickinfopenting() { return navigasiinfopenting; }
    public By clickinfopenting2() { return navigasiinfopenting2; }
    public By clickcari() { return tombolcariinfopenting; }
    public By clickreset() { return tombolresetinfopenting; }
    public By isijudulcari() { return judulcari; }
    public By clikdetail() { return tombolDetailInformasi; }


    //Halaman Tambah Informasi Penting
    By judul = By.name("title");
    By kategori = By.name("category_id");
    By kategoripendidikan = By.xpath("//span[contains(.,\'Pekerjaan\')]");
    By target = By.xpath("(//input[@type=\'text\'])[3]");
    By target2 = By.xpath("//span[contains(.,'KAB. BANDUNG')]");
    By linkinfopenting = By.name("link_url");
    By tombolsubmitinfopenting = By.xpath("//button[contains(.,\'Simpan\')]");
    //By clicktinymce = By.id("tinymce");
    By tombolbataltinfopenting = By.xpath("//button[contains(.,\'Batal\')]");

    public By isijudul() { return judul; }
    public By clicktambahinfopenting() { return tombolTambahInfopenting; }
    public By pilihKategori() { return kategori; }
    public By pilihKategori2() { return kategoripendidikan; }
    public By pilihtarget() { return target; }
    public By pilihtarget2() { return target2; }
    public By isilink() { return linkinfopenting; }
    //public By clickmce() { return clicktinymce; }
    public By clicksubmitInfopenting() { return tombolsubmitinfopenting; }
    public By clickbatalInfopenting() { return tombolbataltinfopenting; }

    //Halaman Detail
    By textjudulinformasi = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div/div[2]/div/div/div[2]/div/span");
    public By cekjudul() { return textjudulinformasi; }


}
