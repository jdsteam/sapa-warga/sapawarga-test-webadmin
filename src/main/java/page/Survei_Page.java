package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Survei_Page extends BaseTest {

    public Survei_Page(WebDriver driver) {
        this.driver = driver;
    }
    //Halaman List Survei
    By navigasisurvei = By.xpath("//span[contains(.,\'Kuesioner\')]");
    By navigasisurvei2 = By.xpath("//span[contains(.,\'Survei\')]");
    By tomboltambahsurvei = By.xpath("//button[@type=\'button\']");
    By tomboltambahcari= By.xpath("//span[contains(.,\'Cari\')]");
    By tomboltambahreset = By.xpath("//span[contains(.,\'Reset\')]");
    By fieldcarinamasurvei = By.xpath("//input[@type=\'text\']");
    By fieldstatus = By.xpath("(//input[@type=\'text\'])[2]");
    By fieldstatus2 = By.xpath("//li[contains(.,\'Berakhir\')]");
    By fieldkabkota = By.xpath("(//input[@type=\'text\'])[3]");
    By fieldkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fieldkec = By.xpath("(//input[@type=\'text\'])[4]");
    By fieldkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fieldkeldesa = By.xpath("(//input[@type=\'text\'])[5]");
    By fieldkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    public By kliknavigasi() { return navigasisurvei; }
    public By kliknavigasi2() { return navigasisurvei2; }
    public By kliktambahsurvei() { return tomboltambahsurvei; }
    public By isicarisurvei() { return fieldcarinamasurvei; }
    public By pilihstatus() { return fieldstatus; }
    public By pilihstatus2() { return fieldstatus2; }
    public By pilihkabkota() { return fieldkabkota; }
    public By pilihkabkota2() { return fieldkabkota2; }
    public By pilihkec() { return fieldkec; }
    public By pilihkec2() { return fieldkec2; }
    public By pilihkeldesa() { return fieldkeldesa; }
    public By pilihkeldesa2() { return fieldkeldesa2; }
    public By tombolcarisurvei() { return tomboltambahcari; }
    public By tombolresetcari() { return tomboltambahreset; }

    //Halaman Tambah Pesan
    By fieldtambahkabkota = By.name("kabkota");
    By fieldtambahkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fieldtambahkec = By.name("kecamatan");
    By fieldtambahkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fieldtambahkeldesa = By.name("kelurahan");
    By fieldtambahkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    By fieldtambahrw = By.xpath("(//input[@type=\'text\'])[4]");
    By fieldtambahjudul = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div[2]/form/div/div/div/input");
    By fieldtambahkategori = By.name("category_id");
    By fieldtambahkategori2 = By.xpath("//li[contains(.,\'Infrastruktur\')]");
    By tomboltambahtidak = By.xpath("//span[contains(.,\'Tidak\')]");
    By tomboltambahya = By.xpath("//span[contains(.,\'Ya\')]");
    By fieldurl = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div[2]/form/div[6]/div/div/input");
    By fieldurl2 = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div[2]/form/div[7]/div/div/input");
    By tombolkonftambahya = By.xpath("//button[contains(.,\'Ya\')]");
    By tombolkonftambahbatal = By.xpath("//button[contains(.,\'Batal\')]");
    By tombolsimpandraft = By.xpath("//button[contains(.,\'Simpan sebagai Draft\')]");
    By tombolkirimpesan = By.xpath("//button[contains(.,\'Simpan dan Aktifkan\')]");
    By tombolbatal = By.xpath("//button[contains(.,\'Batal\')]");
    public By tambahpilihkabkota() { return fieldtambahkabkota; }
    public By tambahpilihkabkota2() { return fieldtambahkabkota2; }
    public By tambahpilihkec() { return fieldtambahkec; }
    public By tambahpilihkec2() { return fieldtambahkec2; }
    public By tambahpilihkeldesa() { return fieldtambahkeldesa; }
    public By tambahpilihkeldesa2() { return fieldtambahkeldesa2; }
    public By tambahpilihrw() { return fieldtambahrw; }
    public By isitambahjudul() { return fieldtambahjudul; }
    public By isiurl() { return fieldurl; }
    public By isiurl2() { return fieldurl2; }
    public By pilihtambahkategori() { return fieldtambahkategori; }
    public By pilihtambahkategori2() { return fieldtambahkategori2; }
    public By pilihtidak() { return tomboltambahtidak; }
    public By pilihya() { return tomboltambahya; }
    public By simpandraft() { return tombolsimpandraft; }
    public By kirimpesan() { return tombolkirimpesan; }
    public By konfya() { return tombolkonftambahya; }
    public By konfbatal() { return tombolkonftambahbatal; }
    public By batal() { return tombolbatal; }

    //Hasil Pencarian
    By halamanditemukan = By.xpath("//div[@id='app']/div/div[2]/section/div/div/div/div[3]/div[3]/table/tbody/tr/td[2]");
    By textpollingtidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textpollingditemukan = By.xpath("//td[contains(.,\'Saran dan harapan pengguna Sapawarga\')]");
    By textstatusditemukan = By.xpath("//td[contains(.,\'Berakhir\')]");
    By textkabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By textkecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By textkeldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    public By halaman() { return halamanditemukan; }
    public By surveitidakditemukan() { return textpollingtidakditemukan; }
    public By surveiditemukan() { return textpollingditemukan; }
    public By kabkotaditemukan() { return textkabkotaditemukan; }
    public By kecditemukan() { return textkecditemukan; }
    public By keldesaditemukan() { return textkeldesaditemukan; }
    public By statusditemukan() { return textstatusditemukan; }
}
