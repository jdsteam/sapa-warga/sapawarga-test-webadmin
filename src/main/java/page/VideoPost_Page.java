package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class VideoPost_Page extends BaseTest {

    public VideoPost_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasivideopost = By.xpath("//span[contains(.,\'Informasi\')]");
    By navigasivideopost2 = By.xpath("//span[contains(.,\'Video Post\')]");
    By judulvideo = By.xpath("//input[@type=\'text\']");
    By kategori = By.xpath("(//input[@type=\'text\'])[2]");
    By kategori2 = By.xpath("//span[contains(.,\'Event\')]");
    By tombolcarivideo = By.xpath("//button[contains(.,\'Cari\')]");
    By tombolresetvideo = By.xpath("//button[contains(.,\'Reset\')]");
    By tomboltambahvideo = By.xpath("//span[contains(.,\' Tambah Video Baru\')]");
    public By clickvideo() { return navigasivideopost; }
    public By clickvideo2() { return navigasivideopost2; }
    public By clickcari() { return tombolcarivideo; }
    public By clickreset() { return tombolresetvideo; }
    public By isijudulvideo() { return judulvideo; }
    public By isikategori() { return kategori; }
    public By isikategori2() { return kategori2; }
    public By clicktambahvideopost() { return tomboltambahvideo; }


    //Halaman Tambah Video Post
    By judultambah = By.name("title");
    By kategoritambah = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div/form/div[2]/div/div/div/input");
    By kategoritambah2 = By.xpath("//li[contains(.,\'Lainnya\')]");
    By target = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div/form/div[3]/div/div/div/span/span/i");
    By target2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By setprioritas = By.xpath("//div[@id='app']/div/div[2]/section/div/div/div/div/form/div[5]/div/div/div/span/span/i");
    By setprioritas2 = By.xpath("//li[contains(.,\'5\')]");
    By url = By.name("video_url");
    By tambahvideo = By.xpath("//button[contains(.,\'Tambah Video\')]");
    By batal = By.xpath("//button[contains(.,\'Batal\')]");
    public By isijudul() { return judultambah; }
    public By pilihkategori() { return kategoritambah; }
    public By pilihkategori2() { return kategoritambah2; }
    public By pilihtarget() { return target; }
    public By pilihtarget2() { return target2; }
    public By pilihsetprioritas() { return setprioritas; }
    public By pilihsetprioritas2() { return setprioritas2; }
    public By isiurl() { return url; }
    public By tomboltambahvideo() { return tambahvideo; }
    public By tombolbatal() { return batal; }


    //Hasil Pencarian
    By textvideotidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textvideotidakditemukan2 = By.xpath("//td[contains(.,\'Peresmian launching Chanel Pembayaran PBB Melalui Tokopedia dan Bukalapak\')]");
    By textkategoriditemukan = By.xpath("//td[contains(.,\'Event\')]");
    public By videotidakditemukan() { return textvideotidakditemukan; }
    public By videoditemukan() { return textvideotidakditemukan2; }
    public By kategoriditemukan() { return textkategoriditemukan; }
}
