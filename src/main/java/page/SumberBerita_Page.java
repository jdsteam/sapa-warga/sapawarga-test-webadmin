package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class SumberBerita_Page extends BaseTest {

    public SumberBerita_Page(WebDriver driver) {
        this.driver = driver;
    }

    //Halaman Tambah Sumber Berita
    By navigasi = By.xpath("//span[contains(.,\'Konfigurasi\')]");
    By navigasi2 = By.xpath("//span[contains(.,\'Sumber Berita\')]");
    By fieldsumberberita = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div/form/div/div/div/input");
    By fieldtautanlogo = By.xpath("(//input[@type=\'text\'])[2]");
    By fieldtautan = By.xpath("(//input[@type=\'text\'])[3]");
    By simpan = By.xpath("//button[contains(.,\'Simpan dan Aktifkan\')]");
    By batal = By.xpath("//button[contains(.,\'Batal\')]");
    By tambahsumber = By.xpath("//button[contains(.,\' Tambah Sumber Berita\')]");
    public By navkategori() { return navigasi; }
    public By navkategori2() { return navigasi2; }
    public By isijudulsumber() { return fieldsumberberita; }
    public By isitautanlogo() { return fieldtautanlogo; }
    public By isitautan() { return fieldtautan; }
    public By tomboltambahsumber() { return tambahsumber; }
    public By tombolbatal() { return batal; }
    public By tombolsimpan() { return simpan; }
}
