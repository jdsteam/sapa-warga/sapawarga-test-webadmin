package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Pesan_Page extends BaseTest {

    public Pesan_Page(WebDriver driver) {
        this.driver = driver;
    }
    //Halaman List Pesan
    By navigasipesan = By.xpath("//span[contains(.,\'Pesan Broadcast\')]");
    By tambahpesan = By.xpath("//span[contains(.,\'Tambah Pesan\')]");
    By tomboltambahcari= By.xpath("//span[contains(.,\'Cari\')]");
    By tomboltambahreset = By.xpath("//span[contains(.,\'Reset\')]");
    By fieldcaripesan = By.xpath("//input[@type=\'text\']");
    By fieldstatus = By.xpath("(//input[@type=\'text\'])[2]");
    By fieldstatus2 = By.xpath("//li[contains(.,\'Dipublikasikan\')]");
    By fieldkabkota = By.xpath("(//input[@type=\'text\'])[3]");
    By fieldkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fieldkec = By.xpath("(//input[@type=\'text\'])[4]");
    By fieldkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fieldkeldesa = By.xpath("(//input[@type=\'text\'])[5]");
    By fieldkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
   public By kliknavigasi() { return navigasipesan; }
    public By tomboltambahpesan() { return tambahpesan; }
    public By isicaripesan() { return fieldcaripesan; }
    public By pilihstatus() { return fieldstatus; }
    public By pilihstatus2() { return fieldstatus2; }
    public By pilihkabkota() { return fieldkabkota; }
    public By pilihkabkota2() { return fieldkabkota2; }
    public By pilihkec() { return fieldkec; }
    public By pilihkec2() { return fieldkec2; }
    public By pilihkeldesa() { return fieldkeldesa; }
    public By pilihkeldesa2() { return fieldkeldesa2; }
    public By tombolcaripesan() { return tomboltambahcari; }
    public By tombolresetcari() { return tomboltambahreset; }


    //Halaman Tambah Pesan
    By fieldtambahkabkota = By.name("kabkota");
    By fieldtambahkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fieldtambahkec = By.name("kecamatan");
    By fieldtambahkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fieldtambahkeldesa = By.name("kelurahan");
    By fieldtambahkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    By fieldtambahrw = By.xpath("(//input[@type=\'text\'])[4]");
    By fieldtambahjudul = By.name("title");
    By fieldtambahkategori = By.name("category_id");
    By fieldtambahkategori2 = By.xpath("//li[contains(.,\'Sosialisasi\')]");
    By tomboltambahterjadwal = By.xpath("//span[contains(.,\'Terjadwal\')]");
    By tomboltambahsekarang = By.xpath("//span[contains(.,\'Sekarang\')]");
    By tomboltambahtidak = By.xpath("//span[contains(.,\'Tidak\')]");
    By tomboltambahya = By.xpath("//span[contains(.,\'Ya\')]");
    By tombolkonftambahya = By.xpath("//button[contains(.,\'Ya\')]");
    By tombolkonftambahbatal = By.xpath("//button[contains(.,\'Batal\')]");
    By tombolsimpandraft = By.xpath("//button[contains(.,\'Simpan sebagai Draft\')]");
    By tombolkirimpesan = By.xpath("//button[contains(.,\'Kirim Pesan\')]");
    public By tambahpilihkabkota() { return fieldtambahkabkota; }
    public By tambahpilihkabkota2() { return fieldtambahkabkota2; }
    public By tambahpilihkec() { return fieldtambahkec; }
    public By tambahpilihkec2() { return fieldtambahkec2; }
    public By tambahpilihkeldesa() { return fieldtambahkeldesa; }
    public By tambahpilihkeldesa2() { return fieldtambahkeldesa2; }
    public By tambahpilihrw() { return fieldtambahrw; }
    public By isitambahjudul() { return fieldtambahjudul; }
    public By pilihtambahkategori() { return fieldtambahkategori; }
    public By pilihtambahkategori2() { return fieldtambahkategori2; }
    public By pilihterjadwal() { return tomboltambahterjadwal; }
    public By pilihsekarang() { return tomboltambahsekarang; }
    public By pilihtidak() { return tomboltambahtidak; }
    public By pilihya() { return tomboltambahya; }
    public By simpandraft() { return tombolsimpandraft; }
    public By kirimpesan() { return tombolkirimpesan; }
    public By konfya() { return tombolkonftambahya; }
    public By konfbatal() { return tombolkonftambahbatal; }

    //Hasil Pencarian
    By halamanutama = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div[3]/div[3]/table/tbody/tr/td[2]");
    By textpesantidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textpesanditemukan = By.xpath("//td[contains(.,\'Sosialisasi program kerja Gubernur Jawa Barat\')]");
    By textstatusditemukan = By.xpath("//td[contains(.,\'Dipublikasikan\')]");
    By textkabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By textkecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By textkeldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    public By pesantidakditemukan() { return textpesantidakditemukan; }
    public By pesanditemukan() { return textpesanditemukan; }
    public By kabkotaditemukan() { return textkabkotaditemukan; }
    public By kecditemukan() { return textkecditemukan; }
    public By keldesaditemukan() { return textkeldesaditemukan; }
    public By statusditemukan() { return textstatusditemukan; }
    public By halamanutamaditemukan() { return halamanutama; }

}