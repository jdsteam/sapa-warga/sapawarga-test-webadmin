package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class KelolaPengguna_Page extends BaseTest {

    public KelolaPengguna_Page(WebDriver driver) {
        this.driver = driver;
    }

    //Halaman List Kelola Penguna
    By navigasikelolapengguna = By.xpath("//span[contains(.,\'Kelola Pengguna\')]");
    By navigasikelolapengguna2 = By.xpath("//span[contains(.,\'Semua Pengguna\')]");
    By tomboltambahpengguna = By.xpath("//span[contains(.,\'Tambah Pengguna Baru\')]");
    By namacari = By.xpath("//input[@type=\'text\']");
    By usernamecari = By.xpath("(//input[@type=\'text\'])[2]");
    By nomorcari = By.xpath("(//input[@type='text'])[3]");
    By fieldstatus = By.xpath("(//input[@type='text'])[4]");
    By fieldstatus2 = By.xpath("//span[contains(.,\'Aktif\')]");
    By fieldstatus3 = By.xpath("//span[contains(.,\'Tidak Aktif\')]");
    By tombolcarikelolapengguna = By.xpath("//button[contains(.,\'Cari\')]");
    By tombolresetkelolapengguna = By.xpath("//button[contains(.,\'Reset\')]");

    public By clickkelolapengguna() { return navigasikelolapengguna; }
    public By clickkelolapengguna2() { return navigasikelolapengguna2; }
    public By isinamalengkap() { return namacari; }
    public By isiusername() { return usernamecari; }
    public By isinomor() { return nomorcari; }
    public By pilihstatus() { return fieldstatus; }
    public By pilihstatus2() { return fieldstatus2; }
    public By pilihstatus3() { return fieldstatus3; }
    public By clicktambahpengguna() { return tomboltambahpengguna; }
    public By clickcari() { return tombolcarikelolapengguna; }
    public By clickreset() { return tombolresetkelolapengguna; }


    //Halaman Tambah Pengguna
    By fieldusername = By.xpath("//input[@type=\'text\']");
    By fieldnamalengkap = By.xpath("(//input[@type=\'text\'])[2]");
    By fieldemail = By.xpath("//input[@type=\'email\']");
    By fieldpassword = By.xpath("//input[@type=\'password\']");
    By fieldtelfn = By.xpath("(//input[@type=\'text\'])[3]");
    By fieldconfirmpass = By.xpath("(//input[@type=\'password\'])[2]");
    By fieldrole = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div[2]/form/div[7]/div/div/div/div/div/input");
    By rw = By.xpath("//li[contains(.,\'Admin Desa/Kelurahan\')]");
    By fieldkabkota = By.xpath("(//div[@id=\'app\']/div/div[2]/section/div/div/div[2]/form/div[7]/div[2]/div/div/div/div/input");
    By fieldkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fieldkec = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div[2]/form/div[8]/div/div/div/div/div/input");
    By fieldkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fieldkeldesa = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div[2]/form/div[8]/div[2]/div/div/div/div/input");
    By fieldkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    By fieldrw = By.xpath("(//input[@type=\'text\'])[8]");
    By fieldrt = By.xpath("(//input[@type=\'text\'])[9]");
    By fieldalamat = By.xpath("(//input[@type=\'text\'])[11]");
    By tomboltambahuser = By.xpath("//button[contains(.,\'Tambah Pengguna\')]");

    public By isitambahusername() { return fieldusername; }
    public By isitambahnamalengkap() { return fieldnamalengkap; }
    public By isiemail() { return fieldemail; }
    public By isipassword() { return fieldpassword; }
    public By isiconfmpass() { return fieldconfirmpass; }
    public By isitelfn() { return fieldtelfn; }
    public By pilihrole() { return fieldrole; }
    public By pilihrole2() { return rw; }
    public By pilihkabkota() { return fieldkabkota; }
    public By pilihkabkota2() { return fieldkabkota2; }
    public By pilihkec() { return fieldkec; }
    public By pilihkec2() { return fieldkec2; }
    public By pilihkeldesa() { return fieldkeldesa; }
    public By pilihkeldesa2() { return fieldkeldesa2; }
    public By isirw() { return fieldrw; }
    public By isirt() { return fieldrt; }
    public By isialamat() { return fieldalamat; }
    public By pilihtomboltambah() { return tomboltambahuser; }

    //Hasil Pencarian
    By halamantambahpengguna = By.xpath("//p[contains(.,\'Tambah Pengguna Baru\')]");
    By textusertidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textuserditemukan = By.xpath("//td[contains(.,\'robotkab.jds\')]");
    By textstatusditemukan = By.xpath("//td[contains(.,\'Aktif\')]");
    By textkabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By textkecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By textkeldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    By textnamalengkapditemukan  = By.xpath("//td[contains(.,\'Robot Kabupaten Kota Pertama\')]");
    By textnotelpditemukan  = By.xpath("//td[contains(.,\'081233333111\')]");
    public By halamanditemukan() { return halamantambahpengguna; }
    public By userditemukan() { return textuserditemukan; }
    public By statusditemukan() { return textstatusditemukan; }
    public By usertidakditemukan() { return textusertidakditemukan; }
    public By namalengkapditemukan() { return textnamalengkapditemukan; }
    public By notelpditemukan() { return textnotelpditemukan; }
    public By kabkotaditemukan() { return textkabkotaditemukan; }
    public By kecditemukan() { return textkecditemukan; }
    public By keldesaditemukan() { return textkeldesaditemukan; }
}
