package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Kategori_Page extends BaseTest {

    public Kategori_Page(WebDriver driver) {
        this.driver = driver;
    }

    //Halaman List Informasi Penting
    By navigasi = By.xpath("//span[contains(.,\'Konfigurasi\')]");
    By navigasi2 = By.xpath("//span[contains(.,\'Kategori\')]");
    By kategoricari = By.xpath("//input[@type=\'text\']");
    By cari = By.xpath("//button[contains(.,\'Cari\')]");
    By reset = By.xpath("//button[contains(.,\'Reset\')]");
    By tambahkategori = By.xpath("//span[contains(.,\' Tambah Kategori Baru\')]");
    By detailkategori = By.xpath("(//button[@type=\'button\'])[4]");
    public By navkategori() { return navigasi; }
    public By navkategori2() { return navigasi2; }
    public By tombolcari() { return cari; }
    public By tombolreset() { return reset; }
    public By isikategoricari() { return kategoricari; }
    public By tomboltambahkategori() { return tambahkategori; }


    //Halaman Tambah Kategori
    By fieldkategori = By.name("category-name");
    By fieldfitur = By.name("fitur-select");
    By fieldfitur2 = By.xpath("//li[7]");
    By simpan = By.xpath("//button[contains(.,\'Simpan dan Aktifkan\')]");
    By batal = By.xpath("//button[contains(.,\'Batal\')]");
    public By isikategori() { return fieldkategori; }
    public By pilihfitur() { return fieldfitur; }
    public By pilihfitur2() { return fieldfitur2; }
    public By tombolsimpan() { return simpan; }
    public By tombolbatal() { return batal; }


    //Hasil Pencarian
    By tidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By ditemukan = By.xpath("//td[contains(.,\'Sosialisasi\')]");
    public By texttidaktemukan() { return tidakditemukan; }
    public By texttemukan() { return ditemukan; }

}