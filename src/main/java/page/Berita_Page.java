package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Berita_Page extends BaseTest {

    public Berita_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasi = By.xpath("//span[contains(.,\'Informasi\')]");
    By navigasi2 = By.xpath("//span[contains(.,\'Berita\')]");
    By judulberita = By.xpath("//input[@type=\'text\']");
    By cari = By.xpath("//button[contains(.,\'Cari\')]");
    By reset = By.xpath("//button[contains(.,\'Reset\')]");
    By tambahberita = By.xpath("//span[contains(.,\' Tambah berita baru\')]");
    By prioritasberita = By.xpath("//span[contains(.,\' Prioritas Berita\')]");

    public By navberita() { return navigasi; }
    public By navberita2() { return navigasi2; }
    public By tombolcari() { return cari; }
    public By tombolreset() { return reset; }
    public By isijudulberita() { return judulberita; }
    public By tomboltambahtambahberita() { return tambahberita; }
    public By tombolprioritasberita() { return prioritasberita; }

    //Hasil Pencarian
    By tidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By ditemukan = By.xpath("//td[contains(.,\'Pembebasan Lahan Tol Cisumdawu Terhambat, Ini Alasan Wagub Jabar\')]");
    By statusditemukan = By.xpath("//td[contains(.,\'Aktif\')]");
    By kabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By kecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By keldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    public By texttidakditemukan() { return tidakditemukan; }
    public By textditemukan() { return ditemukan; }
    public By textkabkotaditemukan() { return kabkotaditemukan; }
    public By textkecditemukan() { return kecditemukan; }
    public By textkeldesaditemukan() { return keldesaditemukan; }
    public By textstatusditemukan() { return statusditemukan; }
}
