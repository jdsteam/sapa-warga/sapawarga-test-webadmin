package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Dashboard_Page extends BaseTest {
    public Dashboard_Page(WebDriver driver) {
        this.driver = driver;
    }
    By logosapawarga = By.xpath("//h1[contains(.,\'sapawarga\')]");

    public By getlogo() { return logosapawarga; }

}
