package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Banner_Page extends BaseTest {

    public Banner_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasi = By.xpath("//span[contains(.,\'Notifikasi Pengguna\')]");
    By navigasi2 = By.xpath("//span[contains(.,\'Banner\')]");
    By judulbanner = By.xpath("//input[@type=\'text\']");
    By cari = By.xpath("//button[contains(.,\'Cari\')]");
    By reset = By.xpath("//button[contains(.,\'Reset\')]");
    By kategori = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div[2]/div/form/div/div[2]/div/div/div/div/span/span/i");
    By kategori2 = By.xpath("//li[contains(.,\'Internal\')]");
    By status = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div[2]/div/form/div/div[3]/div/div/div/div/span/span/i");
    By status2 = By.xpath("//span[contains(.,\'Aktif\')]");
    By tambahberita = By.xpath("//span[contains(.,\' Tambah Banner Baru\')]");

    public By navbanner() { return navigasi; }
    public By navbanner2() { return navigasi2; }
    public By tombolcari() { return cari; }
    public By tombolreset() { return reset; }
    public By isijudulbanner() { return judulbanner; }
    public By pilihkategori() { return kategori; }
    public By pilihkategori2() { return kategori2; }
    public By pilihstatus() { return status; }
    public By pilihstatus2() { return status2; }
    public By tomboltambahtambahbanner() { return tambahberita; }


    //Hasil Pencarian
    By tidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By ditemukan = By.xpath("//td[contains(.,\'Banner ulang tahun Jawa Barat\')]");
    By kategoriditemukan = By.xpath("//td[contains(.,\'internal\')]");
    By statusditemukan = By.xpath("//td[contains(.,\'Aktif\')]");
    public By texttidakditemukan() { return tidakditemukan; }
    public By textditemukan() { return ditemukan; }
    public By textkategoriditemukan() { return kategoriditemukan; }
    public By textstatusditemukan() { return statusditemukan; }

    public By selectByIndex(int i) { return status2; }
}
