package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Usulan_Page extends BaseTest {

    public Usulan_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasiusulan = By.xpath("//span[contains(.,\'Aspirasi\')]");
    By navigasiusulan2 = By.xpath("//span[contains(.,\'Usulan Masyarakat\')]");
    By judulusulan = By.xpath("//input[@type=\'text\']");
    By fieldkategori = By.xpath("(//input[@type=\'text\'])[2]");
    By fieldkategori2 = By.xpath("//li[contains(.,\'Infrastruktur\')]");
    By fieldstatus = By.xpath("(//input[@type=\'text\'])[3]");
    By fieldstatus2 = By.xpath("//li[contains(.,\'Tidak Dipublikasikan\')]");
    By fieldkabkota = By.xpath("(//input[@type=\'text\'])[4]");
    By fieldkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fieldkec = By.xpath("(//input[@type=\'text\'])[5]");
    By fieldkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fieldkeldesa = By.xpath("(//input[@type=\'text\'])[6]");
    By fieldkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    By tombolcariusulan = By.xpath("//button[contains(.,\'Cari\')]");
    By tombolresetusulan = By.xpath("//button[contains(.,\'Reset\')]");
    public By clickusulan() { return navigasiusulan; }
    public By clickusulan2() { return navigasiusulan2; }
    public By clickcari() { return tombolcariusulan; }
    public By clickreset() { return tombolresetusulan; }
    public By isijudulusulan() { return judulusulan; }
    public By pilihkategori() { return fieldkategori; }
    public By pilihkategori2() { return fieldkategori2; }
    public By pilihstatus() { return fieldstatus; }
    public By pilihstatus2() { return fieldstatus2; }
    public By pilihkabkota() { return fieldkabkota; }
    public By pilihkabkota2() { return fieldkabkota2; }
    public By pilihkec() { return fieldkec; }
    public By pilihkec2() { return fieldkec2; }
    public By pilihkeldesa() { return fieldkeldesa; }
    public By pilihkeldesa2() { return fieldkeldesa2; }

    //Hasil Pencarian
    By textusulantidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textusulanditemukan = By.xpath("//td[contains(.,\'coba lapor 2\')]");
    By textstatusditemukan = By.xpath("//td[contains(.,\'Tidak Dipublikasikan\')]");
    By textkategoriditemukan = By.xpath("//td[contains(.,\'Infrastruktur\')]");
    By textkabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By textkecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By textkeldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    public By usulantidakditemukan() { return textusulantidakditemukan; }
    public By usulanditemukan() { return textusulanditemukan; }
    public By kabkotaditemukan() { return textkabkotaditemukan; }
    public By kecditemukan() { return textkecditemukan; }
    public By keldesaditemukan() { return textkeldesaditemukan; }
    public By statusditemukan() { return textstatusditemukan; }
    public By kategoriditemukan() { return textkategoriditemukan; }
}
