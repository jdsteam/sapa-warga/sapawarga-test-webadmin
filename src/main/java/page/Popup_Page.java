package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Popup_Page extends BaseTest {

    public Popup_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasipopup = By.xpath("//span[contains(.,\'Notifikasi Pengguna\')]");
    By navigasipopup2 = By.xpath("//span[contains(.,\'Pop-Up Informasi\')]");
    By judulpopup = By.xpath("//input[@type=\'text\']");
    By kategori = By.xpath("(//input[@type=\'text\'])[2]");
    By kategori2 = By.xpath("//span[contains(.,\'Internal\')]");
    By cari = By.xpath("//button[contains(.,\'Cari\')]");
    By reset = By.xpath("//button[contains(.,\'Reset\')]");
    By tambahpopup = By.xpath("//button[contains(.,\' Tambah Pop-Up Informasi Baru\')]");
    public By nav() { return navigasipopup; }
    public By nav2() { return navigasipopup2; }
    public By tombolcari() { return cari; }
    public By tombolreset() { return reset; }
    public By isijudulpopup() { return judulpopup; }
    public By pilihkategori() { return kategori; }
    public By pilihkategori2() { return kategori2; }
    public By tomboltambah() { return tambahpopup; }


    //Halaman Tambah Video Post
    By judul = By.name("title");
    By internal = By.xpath("//span[contains(.,\'Internal\')]");
    By eksternal = By.xpath("//span[contains(.,\'Eksternal\')]");
    By url = By.name("link_url");
    By tambah = By.xpath("//button[contains(.,\'Tambah Pop Up\')]");
    By batal = By.xpath("//button[contains(.,\'Batal\')]");
    public By isijudul() { return judul; }
    public By piliheksternal() { return eksternal; }
    public By pilihinternal() { return internal; }
    public By isiurl() { return url; }
    public By tomboltambahpopup() { return tambah; }
    public By tombolbatal() { return batal; }


    //Hasil Pencarian
    By textpopuptidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textpopuptidakditemukan2 = By.xpath("//td[contains(.,\'Program Unggulan Sapawarga\')]");
    By textkategoriditemukan = By.xpath("//td[contains(.,\'Internal\')]");
    public By popuptidakditemukan() { return textpopuptidakditemukan; }
    public By popupditemukan() { return textpopuptidakditemukan2; }
    public By kategoriditemukan() { return textkategoriditemukan; }
}
