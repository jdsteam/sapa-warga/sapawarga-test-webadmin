package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Notifikasi_Page extends BaseTest {

    public Notifikasi_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasi = By.xpath("//span[contains(.,\'Notifikasi Pengguna\')]");
    By navigasi2 = By.xpath("//div[@id=\'app\']/div/div/div[2]/div/div/ul/div[7]/li/ul/div[2]/a/li/span");
    By judulnotif = By.xpath("//input[@type=\'text\']");
    By cari = By.xpath("//button[contains(.,\'Cari\')]");
    By reset = By.xpath("//button[contains(.,\'Reset\')]");
    By status = By.xpath("(//input[@type=\'text\'])[2]");
    By status2 = By.xpath("//li[contains(.,\'Dipublikasikan\')]");
    By kabkota = By.xpath("(//input[@type=\'text\'])[3]");
    By kabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By kec = By.xpath("(//input[@type=\'text\'])[4]");
    By kec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By keldesa = By.xpath("(//input[@type=\'text\'])[5]");
    By keldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    By tambahnotif = By.xpath("//button[contains(.,\'Tambah Notifikasi\')]");
    public By navnotif() { return navigasi; }
    public By navnotif2() { return navigasi2; }
    public By tomboltambahnotif() { return tambahnotif; }
    public By tombolcari() { return cari; }
    public By tombolreset() { return reset; }
    public By isijudulnotif() { return judulnotif; }
    public By pilihstatus() { return status; }
    public By pilihstatus2() { return status2; }
    public By pilihkotakab() { return kabkota; }
    public By pilihkotakab2() { return kabkota2; }
    public By pilihkec() { return kec; }
    public By pilihkec2() { return kec2; }
    public By pilihkeldesa() { return keldesa; }
    public By pilihkeldesa2() { return keldesa2; }

    //Halaman Tambah Pesan
    By fkabkota = By.name("kabkota");
    By fkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fkec = By.name("kecamatan");
    By fkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fkeldesa = By.name("kelurahan");
    By fkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    By frw = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div/form/div[2]/div/div/input");
    By fjudul = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div[2]/div/form/div/div/div/input");
    By fkategori = By.name("category_id");
    By fkategori2 = By.xpath("//span[contains(.,\'Berita Terbaru\')]");
    By fisipesan = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div[2]/div/form/div[3]/div/div/textarea");
    By ttidak = By.xpath("//span[contains(.,\'Tidak\')]");
    By tya = By.xpath("//span[contains(.,\'Ya\')]");
    By tkonfya = By.xpath("//button[contains(.,\'Ya\')]");
    By tkonfbatal = By.xpath("//button[contains(.,\'Batal\')]");
    By tsimpandraft = By.xpath("//button[contains(.,\'Simpan sebagai Draft\')]");
    By tkirimpesan = By.xpath("//button[contains(.,\'Kirim Pesan\')]");
    public By pilihtambahkabkota() { return fkabkota; }
    public By pilihtambahkabkota2() { return fkabkota2; }
    public By pilihtambahkec() { return fkec; }
    public By pilihtambahkec2() { return fkec2; }
    public By pilihtambahkeldesa() { return fkeldesa; }
    public By pilihtambahkeldesa2() { return fkeldesa2; }
    public By pilihtambahrw() { return frw; }
    public By isitambahjudul() { return fjudul; }
    public By pilihtambahkategori() { return fkategori; }
    public By pilihtambahkategori2() { return fkategori2; }
    public By isipesan() { return fisipesan; }
    public By pilihtidak() { return ttidak; }
    public By pilihya() { return tya; }
    public By simpandraft() { return tsimpandraft; }
    public By kirimpesan() { return tkirimpesan; }
    public By konfya() { return tkonfya; }
    public By konfbatal() { return tkonfbatal; }

    //Hasil Pencarian
    By halamanutama = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div[3]/div[3]/table/tbody/tr/td[2]");
    By tidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By ditemukan = By.xpath("//td[contains(.,\'Survey Baru: Penilaian aplikasi Sapawarga\')]");
    By statusditemukan = By.xpath("//td[contains(.,\'Dipublikasikan\')]");
    By textkabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By textkecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By textkeldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    public By texttidakditemukan() { return tidakditemukan; }
    public By textditemukan() { return ditemukan; }
    public By textstatusditemukan() { return statusditemukan; }
    public By textkabkotaditemukan() { return textkabkotaditemukan; }
    public By textkecditemukan() { return textkecditemukan; }
    public By textkeldesaditemukan() { return textkeldesaditemukan; }
    public By halamanutamaditemukan() { return halamanutama; }
}