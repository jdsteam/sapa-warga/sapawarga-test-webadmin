package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class SaberHoax_Page extends BaseTest {

    public SaberHoax_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasisaberhoax = By.xpath("//span[contains(.,\'Informasi\')]");
    By navigasisaberhoax2 = By.xpath("//span[contains(.,\'Jabar Saber Hoax\')]");
    By judulberitasaber = By.xpath("//input[@type=\'text\']");
    By tombolcariberitasaber = By.xpath("//button[contains(.,\'Cari\')]");
    By fieldstatus = By.xpath("(//input[@type=\'text\'])[2]");
    By fieldstatus2 = By.xpath("//span[contains(.,\'Fakta\')]");
    By tombolresetberitasaber = By.xpath("//button[contains(.,\'Reset\')]");
    By tomboltambahberitasaber = By.xpath("//span[contains(.,\' Tambah Berita Counter Hoax\')]");

    public By clickberita() { return navigasisaberhoax; }
    public By clickberita2() { return navigasisaberhoax2; }
    public By clickcari() { return tombolcariberitasaber; }
    public By clickreset() { return tombolresetberitasaber; }
    public By isijudulberita() { return judulberitasaber; }
    public By clicktomboltambahberita() { return tomboltambahberitasaber; }
    public By clickstatus() { return fieldstatus; }
    public By clickstatus2() { return fieldstatus2; }


    //Hasil Pencarian
    By testberitatidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textberitaditemukan = By.xpath("//td[contains(.,\'KEMENTERIAN LHK BAGIKAN BIBIT TANAMAN SECARA CUMA-CUMA, DENGAN SYARAT TIDAK DIPERJUAL BELIKAN\')]");
    By textstatusditemukan = By.xpath("//td[contains(.,\'Fakta\')]");
    By textkabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By textkecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By textkeldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    public By beritatidakditemukan() { return testberitatidakditemukan; }
    public By beritaditemukan() { return textberitaditemukan; }
    public By kabkotaditemukan() { return textkabkotaditemukan; }
    public By kecditemukan() { return textkecditemukan; }
    public By keldesaditemukan() { return textkeldesaditemukan; }
    public By statusditemukan() { return textstatusditemukan; }
}
