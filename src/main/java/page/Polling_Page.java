package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Polling_Page extends BaseTest {

    public Polling_Page(WebDriver driver) {
        this.driver = driver;
    }
    //Halaman List Pesan
    By navigasipolling = By.xpath("//span[contains(.,\'Kuesioner\')]");
    By navigasipolling2 = By.xpath("//span[contains(.,\'Polling\')]");
    By tomboltambahpolling = By.xpath("//span[contains(.,\'Tambah Polling Baru\')]");
    By tomboltambahcari= By.xpath("//span[contains(.,\'Cari\')]");
    By tomboltambahreset = By.xpath("//span[contains(.,\'Reset\')]");
    By fieldcarinamapolling = By.xpath("//input[@type=\'text\']");
    By fieldstatus = By.xpath("(//input[@type=\'text\'])[2]");
    By fieldstatus2 = By.xpath("//li[contains(.,\'Berakhir\')]");
    By fieldkabkota = By.xpath("(//input[@type=\'text\'])[3]");
    By fieldkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fieldkec = By.xpath("(//input[@type=\'text\'])[4]");
    By fieldkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fieldkeldesa = By.xpath("(//input[@type=\'text\'])[5]");
    By fieldkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    public By kliknavigasi() { return navigasipolling; }
    public By kliknavigasi2() { return navigasipolling2; }
    public By kliktambahpolling() { return tomboltambahpolling; }
    public By isicaripolling() { return fieldcarinamapolling; }
    public By pilihstatus() { return fieldstatus; }
    public By pilihstatus2() { return fieldstatus2; }
    public By pilihkabkota() { return fieldkabkota; }
    public By pilihkabkota2() { return fieldkabkota2; }
    public By pilihkec() { return fieldkec; }
    public By pilihkec2() { return fieldkec2; }
    public By pilihkeldesa() { return fieldkeldesa; }
    public By pilihkeldesa2() { return fieldkeldesa2; }
    public By tombolcaripolling() { return tomboltambahcari; }
    public By tombolresetcari() { return tomboltambahreset; }


    //Halaman Tambah Polling
    By fieldtambahkabkota = By.name("kabkota");
    By fieldtambahkabkota2 = By.xpath("//li[contains(.,\'KAB. BANDUNG\')]");
    By fieldtambahkec = By.name("kecamatan");
    By fieldtambahkec2 = By.xpath("//li[contains(.,\'ARJASARI\')]");
    By fieldtambahkeldesa = By.name("kelurahan");
    By fieldtambahkeldesa2 = By.xpath("//li[contains(.,\'ANCOLMEKAR\')]");
    By fieldtambahrw = By.xpath("//input[@type=\'text\'])[4]");
    By fieldtambahjudul = By.name("title-polling");
    By fieldtambahdeskripsi = By.name("description-polling");
    By fieldtambahpengertian = By.name("excerpt-polling");
    By fieldtambahpertanyaan = By.name("question");
    By fieldtambahkategori = By.name("category_id");
    By fieldtambahkategori2 = By.xpath("//li[contains(.,\'Kesehatan\')]");
    By tomboltambahtidak = By.xpath("//span[contains(.,\'Tidak\')]");
    By tomboltambahya = By.xpath("//span[contains(.,\'Ya\')]");
    By tomboljawabanlain = By.xpath("//button[contains(.,\'Jawaban Lain\')]");
    By fieldjaawbanlain = By.name("answer-option");
    By fieldjaawbanlain2 = By.xpath("(//input[@name=\'answer-option\'])[2]");
    By fieldjaawbanlain3 = By.xpath("(//input[@name=\'answer-option\'])[3]");
    By tombolsimpandraft = By.xpath("//span[contains(.,\'Simpan sebagai Draft\')]");
    By tombolpublikasipolling = By.xpath("//span[contains(.,\'Publikasikan Polling\')]");
    By tombolkonftambahya = By.xpath("//button[contains(.,\'Ya\')]");
    By tombolkonftambahbatal = By.xpath("//button[contains(.,\'Batal\')]");
    By tombolyatidak = By.xpath("//span[contains(.,\'Ya / Tidak\')]");
    By tombolmultiple = By.xpath("//span[contains(.,\'Multiple\')]");
    By tombolcustom = By.xpath("//span[contains(.,\'Custom\')]");
    public By tambahpilihkabkota() { return fieldtambahkabkota; }
    public By tambahpilihkabkota2() { return fieldtambahkabkota2; }
    public By tambahpilihkec() { return fieldtambahkec; }
    public By tambahpilihkec2() { return fieldtambahkec2; }
    public By tambahpilihkeldesa() { return fieldtambahkeldesa; }
    public By tambahpilihkeldesa2() { return fieldtambahkeldesa2; }
    public By tambahpilihrw() { return fieldtambahrw; }
    public By isitambahjudul() { return fieldtambahjudul; }
    public By isitambahpengantar() { return fieldtambahpengertian; }
    public By isitambahdeskripsi() { return fieldtambahdeskripsi; }
    public By pilihtambahkategori() { return fieldtambahkategori; }
    public By pilihtambahkategori2() { return fieldtambahkategori2; }
    public By pilihtidak() { return tomboltambahtidak; }
    public By pilihya() { return tomboltambahya; }
    public By isipertanyaan() { return fieldtambahpertanyaan; }
    public By isijawabancustom() { return fieldjaawbanlain; }
    public By isijawabancustom2() { return fieldjaawbanlain2; }
    public By isijawabancustom3() { return fieldjaawbanlain3; }
    public By pilihyatidak() { return tombolyatidak; }
    public By pilihmultiple() { return tombolmultiple; }
    public By pilihcustom() { return tombolcustom; }
    public By pilihjawabanlain() { return tomboljawabanlain; }
    public By simpandraft() { return tombolsimpandraft; }
    public By publikasikanpolling() { return tombolpublikasipolling; }
    public By konfya() { return tombolkonftambahya; }
    public By konfbatal() { return tombolkonftambahbatal; }

    //Hasil Pencarian
    By halamanutama = By.xpath("//div[@id=\'app\']/div/div[2]/section/div/div/div/div[3]/div[3]/table/tbody/tr/td[2]");
    By textpollingtidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textpollingditemukan = By.xpath("//td[contains(.,\'Poling naon atuh\')]");
    By textstatusditemukan = By.xpath("//td[contains(.,\'Berakhir\')]");
    By textkabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By textkecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By textkeldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    public By pollingtidakditemukan() { return textpollingtidakditemukan; }
    public By pollingditemukan() { return textpollingditemukan; }
    public By kabkotaditemukan() { return textkabkotaditemukan; }
    public By kecditemukan() { return textkecditemukan; }
    public By keldesaditemukan() { return textkeldesaditemukan; }
    public By statusditemukan() { return textstatusditemukan; }
    public By halamanutamaditemukan() { return halamanutama; }

}
