package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class NomorPenting_Page extends BaseTest {

    public NomorPenting_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasinomorpenting = By.xpath("//span[contains(.,\'Informasi\')]");
    By navigasinomorpenting2 = By.xpath("//span[contains(.,\'Nomor Penting\')]");
    By namainstansi = By.xpath("//input[@type=\'text\']");
    By nomortelfn = By.xpath("(//input[@type=\'text\'])[2]");
    By status = By.xpath("(//input[@type=\'text\'])[3]");
    By status2 = By.xpath("Aktif");
    By tombolcarinomorpenting = By.xpath("//button[contains(.,\'Cari\')]");
    By tombolresetnomorpenting = By.xpath("//button[contains(.,\'Reset\')]");
    By tomboltambahnomorpenting = By.xpath("//span[contains(.,\' Tambah Nomor Penting\')]");

    public By clicknomorpenting() { return navigasinomorpenting; }
    public By clicknomorpenting2() { return navigasinomorpenting2; }
    public By clickcari() { return tombolcarinomorpenting; }
    public By clickreset() { return tombolresetnomorpenting; }
    public By isiinstansi() { return namainstansi; }
    public By isitelfn() { return nomortelfn; }
    public By isistatus() { return status; }
    public By isistatus2() { return status2; }
    public By clicktombolnomorpenting() { return tomboltambahnomorpenting; }


    //Hasil Pencarian
    By textnomortidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By textnomorditemukan = By.xpath("//td[contains(.,\'AREN JAYA\')]");
    By textnomorditemukan2 = By.xpath("//td[contains(.,\'02188347036\')]");
    By textstatusditemukan = By.xpath("//td[contains(.,\'Aktif\')]");
    By textkabkotaditemukan = By.xpath("//td[contains(.,\'KAB. BANDUNG\')]");
    By textkecditemukan  = By.xpath("//td[contains(.,\'ARJASARI\')]");
    By textkeldesaditemukan  = By.xpath("//td[contains(.,\'ANCOLMEKAR\')]");
    public By nomortidakditemukan() { return textnomortidakditemukan; }
    public By nomorditemukan() { return textnomorditemukan; }
    public By nomorditemukan2() { return textnomorditemukan2; }
    public By kabkotaditemukan() { return textkabkotaditemukan; }
    public By kecditemukan() { return textkecditemukan; }
    public By keldesaditemukan() { return textkeldesaditemukan; }
    public By statusditemukan() { return textstatusditemukan; }

}
