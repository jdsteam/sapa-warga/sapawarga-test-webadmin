package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class ReleaseManagement_Page extends BaseTest {
    public ReleaseManagement_Page(WebDriver driver) {
        this.driver = driver;
    }

    //Halaman List Release Management
    By tambahsumber = By.xpath("//button[contains(.,\' Tambah Release\')]");
    By edit = By.xpath("//button[contains(.,\'Edit\')]");
    By hapus = By.xpath("//button[contains(.,\'Hapus\')]");
    public By tomboledit() { return edit; }
    public By tombolhapus() { return hapus; }
    public By tomboltambah() { return tambahsumber; }


    //Halaman Tambah Sumber Berita
    By navigasi = By.xpath("//span[contains(.,\'Release Management\')]");
    By versi = By.name("version");
    By force = By.xpath("//span[contains(.,\'Ya\')]");
    By tidakforce = By.xpath("//span[contains(.,\'Tidak\')]");
    By simpan = By.xpath("//button[contains(.,\'Simpan\')]");
    By batal = By.xpath("//button[contains(.,\'Batal\')]");
    public By nav() { return navigasi; }
    public By isiversi() { return versi; }
    public By pilihya() { return force; }
    public By pilihtidak() { return tidakforce; }
    public By tombolbatal() { return batal; }
    public By tombolsimpan() { return simpan; }
}
