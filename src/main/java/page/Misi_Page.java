package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.BaseTest;

public class Misi_Page extends BaseTest {

    public Misi_Page(WebDriver driver) {
        this.driver = driver;
    }

    By navigasi = By.xpath("//span[contains(.,\'Misi\')]");
    By judulmisi = By.xpath("//input[@type=\'text\']");
    By cari = By.xpath("//button[contains(.,\'Cari\')]");
    By reset = By.xpath("//button[contains(.,\'Reset\')]");
    public By navmisi(){ return navigasi; }
    public By tombolcari() { return cari; }
    public By tombolreset() { return reset; }
    public By isijudulmisi() { return judulmisi; }

    //Hasil Pencarian
    By tidakditemukan = By.xpath("//span[contains(.,\'Tidak ada data\')]");
    By ditemukan = By.xpath("//td[contains(.,\'Misi info penting\')]");
    public By texttidakditemukan() { return tidakditemukan; }
    public By textditemukan() { return ditemukan; }
}