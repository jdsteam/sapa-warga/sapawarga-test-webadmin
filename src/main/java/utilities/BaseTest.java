package utilities;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.NoSuchElementException;

import net.bytebuddy.asm.Advice;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.HashMap;

//import io.qameta.allure.Attachment;

public class BaseTest {

	public WebDriver driver;
	private Properties CONFIG = null;
	private String configFileName = System.getProperty("user.dir")
			+ "\\src\\main\\java\\data\\config.properties";
	
	/**
	 * Click on the element By xpath
	 * 
	 * @return
	 * @throws Exception
	 */
	public void click(By ObjectName) throws Exception {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(ObjectName));
		if (driver.findElement(ObjectName).isEnabled()) {
			driver.findElement(ObjectName).click();
		} else {
			throw new NoSuchElementException();
		}
	}

	/**
	 * Verify the element by the xpath
	 * 
	 * @return true or false
	 * @throws Exception
	 */
	public boolean verify(By ObjectName) throws Exception {
		boolean enabled = false;
		new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(ObjectName));
		if (driver.findElement(ObjectName).isEnabled()) {

			enabled = true;
		} else {
			throw new NoSuchElementException();
		}
		return enabled;
	}
	public boolean visible(By ObjectName) throws Exception {
		boolean enabled = false;
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(ObjectName));
		if (driver.findElement(ObjectName).isDisplayed()) {

			enabled = true;
		} else {
			throw new NoSuchElementException();
		}
		return enabled;
	}

		public String getText(By ObjXpath) throws Exception {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(ObjXpath));
		return driver.findElement(ObjXpath).getText();
	}

	public void waitApp(By ObjectName) throws Exception{
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(ObjectName));
	}

	/**
	 * Launch the browser
	 */
	public void launchBrowser() {

	}

	public void navigate() {

	}

	/**
	 * Type on the element By xpath
	 * 
	 * @return
	 * @throws Exception
	 */
	public void type(By ObjectName, String value) throws Exception {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(ObjectName));
		if (driver.findElement(ObjectName).isEnabled() & textHasContent(value)) {
			driver.findElement(ObjectName).sendKeys(value);
		} else {
			throw new NoSuchElementException();
		}
	}

	public void typeMce(String value) throws Exception{
		driver.switchTo().frame(0);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.getElementById(\"tinymce\").innerHTML=\'"+value+"\';");
		driver.findElement(By.id("tinymce")).click();
		driver.switchTo().defaultContent();
	}

	public String typeRandomSimpan(By ObjectName, String value, String simpan ) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, Object> vars = new HashMap<String, Object>();
		vars.put("Randomkeun", js.executeScript("return new Date().getMilliseconds()"));
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(ObjectName));
		if (driver.findElement(ObjectName).isEnabled() & textHasContent(value)) {
			driver.findElement(ObjectName).sendKeys(value+" "+vars.get("Randomkeun").toString());
			if (simpan.equals("simpan")) {vars.put("penyimpanan",value+" "+vars.get("Randomkeun").toString());}
		} else {
			throw new NoSuchElementException();
		}
		return vars.get("penyimpanan").toString();
	}

	public void typeRandom(By ObjectName, String value) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, Object> vars = new HashMap<String, Object>();
		vars.put("Randomkeun", js.executeScript("return new Date().getMilliseconds()"));
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(ObjectName));
		if (driver.findElement(ObjectName).isEnabled() & textHasContent(value)) {
			driver.findElement(ObjectName).sendKeys(value + "" + vars.get("Randomkeun").toString());
		} else {
			throw new NoSuchElementException();
		}
	}
	public void typeRandomEmail(By ObjectName, String value) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, Object> vars = new HashMap<String, Object>();
		vars.put("Randomkeun", js.executeScript("return new Date().getMilliseconds()"));
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(ObjectName));
		if (driver.findElement(ObjectName).isEnabled() & textHasContent(value)) {
			driver.findElement(ObjectName).sendKeys(vars.get("Randomkeun").toString()+""+value);
		} else {
			throw new NoSuchElementException();
		}
	}

	public void storeText(String variable) throws Exception {

//		HashMap<String, Object> vars = new HashMap<String, Object>();
//		vars.put(variable, "penyimpanan");
//		if (variable.equals("get")) { vars.put(variable, vars.get(ObjectName).toString()); }
//		{ throw new Exception("inputan salah");	}
	}

//	public void randomchar(String value) throws Exception{
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		HashMap<String, Object> vars = new HashMap<String, Object>();
//		vars.put(value, js.executeScript("return new Date().getMilliseconds()"));
//		driver.findElement(By.xpath("//input[@type=\'text\']")).sendKeys(vars.get("Coba").toString());
//	}
	/**
	 * Navigates to the previous screen
	 */
	public void navigate_to_previousScreen() {
		driver.navigate().back();
	}

	/**
	 * Returns true if aText is non-null and has visible content.
	 * 
	 * @param aText is String
	 * @return true or false
	 */
	public boolean textHasContent(String aText) {
		String EMPTY_STRING = "";
		return (aText != null) && (!aText.trim().equals(EMPTY_STRING));
	}

	/**
	 * This method waits for 3 seconds
	 * 
	 * @throws Exception Interrupted Exception
	 */
	public void wait_Low() throws Exception {
		Thread.sleep(3000);
	}

	/**
	 * This method waits for 7 seconds
	 * 
	 * @throws Exception Interrupted Exception
	 */
	public void wait_Medium() throws Exception {
		Thread.sleep(7000);
	}

	/**
	 * This method waits for 10 seconds
	 * 
	 * @throws Exception Interrupted Exception
	 */
	public void wait_High() throws Exception {
		Thread.sleep(10000);
	}

	/**
	 * Scrolls till Footer
	 */
	public void scrollTillFooter() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		Thread.sleep(2000);
	}

	/**
	 * Scrolls till middle
	 */
	public void scrollTillMiddle() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(500,500)");
		Thread.sleep(2000);
	}

	/**
	 * Scrolls till game section
	 */
	public void scrollTillGame() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(50,950)");
		Thread.sleep(2000);
	}

		/**
	 * Scrolls till Footer
	 */
	public void scrollTillUp() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(1000,0)");
		Thread.sleep(2000);
	}

	public void LoginSW(String roleSW) throws Exception {
//		return roleSW;
		if (roleSW.equals("Admin")){
			driver.findElement(By.name("username")).sendKeys("admin");
			driver.findElement(By.name("password")).sendKeys("123456");
			driver.findElement(By.name("password")).sendKeys(Keys.ENTER);
		} else if (roleSW.equals("prov")) {
			driver.findElement(By.name("username")).sendKeys("robotprov.jds");
			driver.findElement(By.name("password")).sendKeys("123456");
			driver.findElement(By.name("password")).sendKeys(Keys.ENTER);
		} else if (roleSW.equals("hoax")) {
			driver.findElement(By.name("username")).sendKeys("robothoax.jds");
			driver.findElement(By.name("password")).sendKeys("123456");
			driver.findElement(By.name("password")).sendKeys(Keys.ENTER);
		} else {
			throw new Exception("Role ditemukan");
		}
	}

	/*public void Login() throws Exception {
		driver.findElement(By.xpath("//div[2]/div[6]/a[text()='LOGIN /']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//img[@src='assets/images/icons/icon-fb.png']")).click();// driver.findElement(By.xpath("//div[1]/div/input")).sendKeys("");
		Thread.sleep(3000);
		driver.findElement(By.id("email")).sendKeys("85280109871");
		driver.findElement(By.id("pass")).sendKeys("DuniaGames6");
		driver.findElement(By.id("loginbutton")).click();
	}*/

	@BeforeClass
	public void setUp() throws Exception {
		CONFIG = new Properties();
		FileInputStream fsconf = new FileInputStream(configFileName);
		CONFIG.load(fsconf);
		String browser = CONFIG.getProperty("browser");
		
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/Libs/1.exe");
			driver = new ChromeDriver();
			driver.get(CONFIG.getProperty("testURL"));
			driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/Libs/geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.get(CONFIG.getProperty("testURL"));
		} else if (browser.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "/Libs/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.get(CONFIG.getProperty("testURL"));
			driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("chromeHeadless")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/Libs/1.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			options.addArguments("--no-sandbox");
			options.addArguments("window-size=1200x600");
			driver = new ChromeDriver(options);
			driver.get(CONFIG.getProperty("testURL"));
			driver.manage().window().maximize();
		} else {
			throw new Exception("Browser is not correct");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

//	@Attachment(value = "Page screenshot", type = "image/png")
//	public byte[] saveScreenshotPNG(WebDriver driver) throws Exception {
//		Thread.sleep(6000);
//		return (((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
//	}
}
